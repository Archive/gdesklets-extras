s srcdir = .

SENSORS = \
	ACPI \
	BugZilla \
	Clock \
	gDeskCal \
	RSS \
	StarterBar \
	SysInfo \
	TestSensor \
	Weather \
	Wireless \
	XMMS

all: warn-maybe-missing list-available-targets

clean: sensors-clean

list-available-targets:
	@echo "Available targets:"
	@echo "sensors:         build all package flavors (bin, bip2, gzip)"
	@echo "sensors-bin:     build binary installers (bin)"
	@echo "sensors-bz2:     build tarballs (bzip2)"
	@echo "sensors-gz:      build tarballs (gzip)"
	@echo "sensors-clean:   remove all packages"
	@echo "sensors-install: install all sensors"

sensors: warn-maybe-missing sensors-prepare sensors-bin-real sensors-bz2-real \
	 sensors-gz-real

extract-po:
	@for i in $(SENSORS); do \
		if test -d Sensors/$$i/po; then \
			domain=`make -s -f Sensors/$$i/po/Makefile return-domain`; \
			echo $$domain; \
			( cd Sensors/$$i/po && intltool-update -p -g $$domain ); \
			for j in `cd po && ls *.po`; do \
				langcode=`echo $$j | sed 's/\(.*\)\.po/\1/'`; \
				msgmerge -o Sensors/$$i/po/$$langcode.po \
				po/$$langcode.po Sensors/$$i/po/$$domain.pot; \
			done; \
		fi; \
	done;

sensors-prepare: extract-po
	@for i in $(SENSORS); do \
		if test -d Sensors/$$i/po; then \
			if test '!' -d Sensors/$$i/locale; then \
				mkdir Sensors/$$i/locale; \
				chmod 0755 Sensors/$$i/locale; \
			fi; \
			python ../gdesklets/locale/install_locales.py \
			gdesklets Sensors/$$i/po Sensors/$$i/locale; \
		fi; \
	done; \

sensors-bin: warn-maybe-missing sensors-prepare sensors-bin-real

sensors-bin-real:
	@echo "Building binary installers..."
	@for i in $(SENSORS); do \
		echo "."; \
		./pack_sensor.py Sensors/$$i; \
	done;

sensors-bz2: warn-maybe-missing sensors-prepare sensors-bz2-real

sensors-bz2-real:
	@echo -n "Building bzip2 tarballs"
	@for i in $(SENSORS); do \
		echo -n "."; \
		tar -cj --exclude=CVS --exclude=Makefile --exclude=*.pyc \
		--exclude .cvsignore --exclude '*~' --exclude .xvpics \
		--exclude po -f $$i.tar.bz2 Sensors/$$i; \
	done;
	@echo
	@echo "Finished building bzip2 tarballs."

sensors-gz: warn-maybe-missing sensors-prepare sensors-gz-real

sensors-gz-real:
	@echo -n "Building gzip tarballs"
	@for i in $(SENSORS); do \
		echo -n "."; \
		tar -cz --exclude=CVS --exclude=Makefile --exclude=*.pyc \
		--exclude .cvsignore --exclude '*~' --exclude .xvpics \
		--exclude po -f $$i.tar.gz Sensors/$$i; \
	done;
	@echo
	@echo "Finished building gzip tarballs."

sensors-clean:
	@echo -n "Removing all packages, cleaning up source directories"
	@for i in $(SENSORS); do \
		echo -n "."; \
		for j in `cd po && ls *.po`; do \
			rm -f Sensors/$$i/po/$$j; \
		done; \
		if test -d Sensors/$$i/locale; then \
			rm -rf Sensors/$$i/locale; \
		fi; \
		rm -f Install_$$i\_Sensor.bin $$i.tar.bz2 $$i.tar.gz; \
	done;
	@echo
	@echo "Finished removing all packages."

sensors-install: warn-maybe-missing sensors-prepare sensors-install-real

sensors-install-real:
	@if [ -z `pkg-config --exists gdesklets-core` ]; then \
	echo "Looking for gDesklets installation."; \
		coredir=`pkg-config --variable=prefix gdesklets-core`/share/gdesklets; \
		echo -n "Installing into $$coredir"; \
		if test '!' -d $$coredir/Sensors; then \
			mkdir $$coredir/Sensors; \
		fi; \
		for i in $(SENSORS); do \
			echo -n "."; \
			if test '!' -d $$coredir/Sensors/$$i; then \
				mkdir $$coredir/Sensors/$$i; \
			fi; \
			installdirs=`find Sensors/$$i -type d ! -path "*CVS*"`; \
			for j in $$installdirs; do \
				if test '!' -d $$coredir/$$j; then \
					mkdir $$coredir/$$j; \
				fi; \
			done; \
			installfiles=`find Sensors/$$i ! -type d ! -path "*CVS*" ! -name ".#*" ! -name ".*.swp"`; \
			for j in $$installfiles; do \
				cp --preserve=mode $$j $$coredir/$$j; \
			done; \
		done; \
		echo; \
		echo "Finished Sensor installation."; \
	fi;

warn-maybe-missing:
	@if test '!' -d ../gdesklets; then \
		echo "WARNING"; \
		echo; \
		echo "You seem to lack the ../gdesklets directory. Build will most likely fail."; \
		echo "Refer to README.i18n for further information."; \
		echo; \
	elif test '!' -f ../gdesklets/locale/install_locales.py; then \
		echo "WARNING"; \
		echo; \
		echo "../gdesklets/locale/install_locales.py doesn't seem to exist. Either your tarball/checkout is outdated or ../gdesklets is no valid gDesklets directory. Build will most likely fail."; \
		echo "Refer to README.i18n for further information."; \
		echo; \
	fi;
