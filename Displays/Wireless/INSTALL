These are generic installation instructions:
--------------------------------------------

Displays (desktop applets) don't have to be installed to be used. You may run
them where they are (you decide where you collect your displays).

Sensors, however, have to be installed before displays can use them. If a
required sensor is not installed, then 'gDesklets' will complain that it
couldn't find the sensor.
You just have to run the sensor installer in order to install a sensor.


For example, if you have just downloaded and unpacked the package 'Foo', then
this is what you have:

  Foo/
    + gfx/
    + foo.display
    + Install_Foo_Sensor.bin
    + INSTALL
    + README

The package consists of the 'foo.display' along with some graphics in the 
subdirectory 'gfx/'. In order to run, the 'foo.display' requires the 'Foo'
Sensor, which came along with the display.
By running 'Install_Foo_Sensor.bin' you can install the sensor and
'foo.display' will work.
You may then delete 'Install_Foo_Sensor.bin' if you like.
