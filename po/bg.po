# Bulgarian translation of gdesklets-extras po file.
# Copyright (C) 2005 THE gdesklets-extras COPYRIGHT HOLDER
# This file is distributed under the same license as the gdesklets-extras package.
# Alexander Shopov <ash@contact.bg>, 2005.
#
msgid ""
msgstr ""
"Project-Id-Version: gdesklets-extras HEAD\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2005-12-13 09:13+0200\n"
"PO-Revision-Date: 2005-12-13 09:01+0200\n"
"Last-Translator: Alexander Shopov <ash@contact.bg>\n"
"Language-Team: Bulgarian <dict@fsa-bg.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: ../Sensors/Clock/__init__.py:47
msgid "local"
msgstr "местно"

#: ../Sensors/Clock/__init__.py:48
msgid "UTC"
msgstr "по Гринуич"

#: ../Sensors/Clock/__init__.py:94
msgid "Clock"
msgstr "Часовник"

#: ../Sensors/Clock/__init__.py:95
msgid "Clock Settings"
msgstr "Настройки на часовника"

#: ../Sensors/Clock/__init__.py:96
msgid "_Show seconds"
msgstr "Показване на _секундите"

#: ../Sensors/Clock/__init__.py:97
msgid "Toggles the seconds on and off"
msgstr "Превключване на показването на секундите"

#: ../Sensors/Clock/__init__.py:98
msgid "Show _date"
msgstr "Показване на _датата"

#: ../Sensors/Clock/__init__.py:99
msgid "Toggles the date on and off"
msgstr "Превключване на показването на датата"

#: ../Sensors/Clock/__init__.py:100
msgid "Display mode (digital clock)"
msgstr "Режим на визуализация (електронен часовник)"

#: ../Sensors/Clock/__init__.py:101
msgid "12h or 24h display mode"
msgstr "12/24-часов режим"

#: ../Sensors/Clock/__init__.py:102
msgid "12h"
msgstr "12-часов"

#: ../Sensors/Clock/__init__.py:103
msgid "24h"
msgstr "24-часов"

#: ../Sensors/Clock/__init__.py:105
msgid "Regional Settings"
msgstr "Регионални настройки"

#: ../Sensors/Clock/__init__.py:106
msgid "Timezone"
msgstr "Часови пояс"

#: ../Sensors/Clock/__init__.py:107
msgid "The timezone to use"
msgstr "Часовият пояс, който да се ползва"

#: ../Sensors/RSS/__init__.py:146
msgid "RSS"
msgstr "RSS"

#: ../Sensors/RSS/__init__.py:147
msgid "RSS Source URL"
msgstr "Източник на емисия по RSS"

#: ../Sensors/RSS/__init__.py:148
msgid "URL"
msgstr "URL"

#: ../Sensors/RSS/__init__.py:148
msgid "The URL of the RSS feed"
msgstr "URL-то за източника на RSS"

#: ../Sensors/RSS/__init__.py:149
msgid "Interval between RSS updates (minutes)"
msgstr "На колко минути да се обновяват емисиите"

#: ../Sensors/RSS/__init__.py:150
msgid "Interval"
msgstr "Обновяване на:"

#: ../Sensors/RSS/__init__.py:151
msgid "The interval between updates (minutes)"
msgstr "Борят минути между обновяванията"

#: ../Sensors/RSS/__init__.py:152
msgid "Font Settings"
msgstr "Настройки на шрифта"

#: ../Sensors/RSS/__init__.py:153
msgid "List font:"
msgstr "Шрифт за списъка:"

#: ../Sensors/RSS/__init__.py:154
msgid "Font used in the URL list"
msgstr "Шрифт, който да се ползва в списъка с URL-та"

#: ../Sensors/RSS/__init__.py:155
msgid "List font color:"
msgstr "Цвят на шрифта за списъка:"

#: ../Sensors/RSS/__init__.py:156
msgid "Color used in the URL list"
msgstr "Цвят за списъка с URL-та"

#: ../Sensors/RSS/__init__.py:157
msgid "Title font:"
msgstr "Шрифт за заглавието:"

#: ../Sensors/RSS/__init__.py:158
msgid "Font used in the title"
msgstr "Шрифт използван в заглавието"

#: ../Sensors/RSS/__init__.py:159
msgid "Title font color:"
msgstr "Цвят на шрифта за заглавието:"

#: ../Sensors/RSS/__init__.py:160
msgid "Color used in the title"
msgstr "Цвят за заглавието"

#: ../Sensors/RSS/__init__.py:161
msgid "Highlight font:"
msgstr "Шрифт за открояване:"

#: ../Sensors/RSS/__init__.py:162
msgid "Font used to highlight link"
msgstr "Шрифтът за открояване на връзка"

#: ../Sensors/RSS/__init__.py:163
msgid "Highlight font color:"
msgstr "Цвят на шрифта за открояване:"

#: ../Sensors/RSS/__init__.py:165
msgid "Color used to highlight link"
msgstr "Цветът на шрифта за открояване на връзка"

#: ../Sensors/StarterBar/__init__.py:627
msgid "Panel"
msgstr "Панел"

#: ../Sensors/StarterBar/__init__.py:628
msgid "Appearance"
msgstr "Външен вид"

#: ../Sensors/StarterBar/__init__.py:629
msgid "Orientation:"
msgstr "Разположение:"

#: ../Sensors/StarterBar/__init__.py:630
msgid "The orientation of the panel"
msgstr "Разположението на панела"

#: ../Sensors/StarterBar/__init__.py:631
msgid "top"
msgstr "горна"

#: ../Sensors/StarterBar/__init__.py:632
msgid "bottom"
msgstr "долна"

#: ../Sensors/StarterBar/__init__.py:633
msgid "left"
msgstr "лява"

#: ../Sensors/StarterBar/__init__.py:634
msgid "right"
msgstr "дясна"

#: ../Sensors/StarterBar/__init__.py:636
msgid "Zoom:"
msgstr "Мащабиране:"

#: ../Sensors/StarterBar/__init__.py:637
msgid "The size of the panel in percents"
msgstr "Размерът на панела в проценти"

#: ../Sensors/StarterBar/__init__.py:639
msgid "Show animations"
msgstr "Показване на анимациите"

#: ../Sensors/StarterBar/__init__.py:640
msgid "Toggles eye candy animations on or off"
msgstr "Превключване на показването на анимациите"

#: ../Sensors/StarterBar/__init__.py:641
msgid "Stretch panel (only when animated)"
msgstr "Разтягане на панела (само при анимациите)"

#: ../Sensors/StarterBar/__init__.py:643
msgid "Toggles stretching of the panel on or off"
msgstr "Превключване на разтягането на панела"

#: ../Sensors/StarterBar/__init__.py:645
msgid "Show captions (only in horizontal orientation)"
msgstr "Показване на заглавията (само при хоризонтално разположение)"

#: ../Sensors/StarterBar/__init__.py:647
msgid "Toggles captions on or off"
msgstr "Превключване на показването на заглавия"

#: ../Sensors/StarterBar/__init__.py:648
msgid "Caption Font:"
msgstr "Шрифт на заглавията:"

#: ../Sensors/StarterBar/__init__.py:649
msgid "Selects the caption's font."
msgstr "Шрифтът за заглавията."

#: ../Sensors/StarterBar/__init__.py:650
msgid "Caption Color:"
msgstr "Цвят на заглавията:"

#: ../Sensors/StarterBar/__init__.py:651
msgid "Selects the caption's color."
msgstr "Цветът на заглавията."

#: ../Sensors/StarterBar/__init__.py:652 ../Sensors/SysInfo/__init__.py:57
msgid "Background:"
msgstr "Фон:"

#: ../Sensors/StarterBar/__init__.py:653 ../Sensors/SysInfo/__init__.py:58
msgid "Selects the background image file."
msgstr "Използваното изображение за фон."

#: ../Sensors/StarterBar/__init__.py:654
msgid "Foreground:"
msgstr "Преден план:"

#: ../Sensors/StarterBar/__init__.py:655
msgid "Selects the foreground image file."
msgstr "Използваното изображение за преден план."

#: ../Sensors/SysInfo/__init__.py:55
msgid "System Information Desklet"
msgstr "Плотама за системна информация"

#: ../Sensors/SysInfo/__init__.py:56
msgid "Settings"
msgstr "Настройки"

#: ../Sensors/SysInfo/__init__.py:59
msgid "Font:"
msgstr "Шрифт:"

#: ../Sensors/SysInfo/__init__.py:59
msgid "The font which is used to display the labels"
msgstr "Шрифтът, който се използва за етикетите"

#: ../Sensors/SysInfo/__init__.py:61
msgid "Key color:"
msgstr "Цвят на ключа:"

#: ../Sensors/SysInfo/__init__.py:62
msgid "The color of the key"
msgstr "Цветът на ключа"

#: ../Sensors/SysInfo/__init__.py:63
msgid "Value color:"
msgstr "Цвят на отчетите:"

#: ../Sensors/SysInfo/__init__.py:64
msgid "The color of the value"
msgstr "Цветът за отчетите"

#: ../Sensors/SysInfo/__init__.py:65
msgid "Title color:"
msgstr "Цвят на заглавията:"

#: ../Sensors/SysInfo/__init__.py:66
msgid "The color of the titles"
msgstr "Цветът за заглавията"

#: ../Sensors/SysInfo/__init__.py:67
msgid "Partition:"
msgstr "Дял:"

#: ../Sensors/SysInfo/__init__.py:69
msgid "Memory:"
msgstr "Памет:"

#: ../Sensors/SysInfo/__init__.py:72
msgid "Network:"
msgstr "Мрежа:"

#: ../Sensors/SysInfo/__init__.py:99
msgid "Kernel:"
msgstr "Ядро:"

#: ../Sensors/SysInfo/__init__.py:100 ../Sensors/SysInfo/__init__.py:110
msgid "Load:"
msgstr "Натоварване:"

#: ../Sensors/SysInfo/__init__.py:101
msgid "Users:"
msgstr "Потребители:"

#: ../Sensors/SysInfo/__init__.py:102
msgid "Uptime:"
msgstr "Непрекъсната работа:"

#: ../Sensors/SysInfo/__init__.py:103
msgid "Idle time:"
msgstr "Време на престой:"

#: ../Sensors/SysInfo/__init__.py:107
msgid "Clock:"
msgstr "Часовник:"

#: ../Sensors/SysInfo/__init__.py:108
msgid "Bogomips:"
msgstr "Bogomips:"

#: ../Sensors/SysInfo/__init__.py:109
msgid "Cache:"
msgstr "Кеш:"

#: ../Sensors/SysInfo/__init__.py:114 ../Sensors/SysInfo/__init__.py:122
msgid "Total:"
msgstr "Общо:"

#: ../Sensors/SysInfo/__init__.py:115 ../Sensors/SysInfo/__init__.py:123
msgid "Used:"
msgstr "Използвана:"

#: ../Sensors/SysInfo/__init__.py:116 ../Sensors/SysInfo/__init__.py:124
msgid "Free:"
msgstr "Свободна:"

#: ../Sensors/SysInfo/__init__.py:117
msgid "Buffer:"
msgstr "Буфери:"

#: ../Sensors/SysInfo/__init__.py:118
msgid "Cached:"
msgstr "Кеширана:"

#: ../Sensors/Weather/__init__.py:129
msgid "Weather"
msgstr "Времето"

#: ../Sensors/Weather/__init__.py:130
msgid "Server Settings"
msgstr "Настройки за сървъра"

#. translateable strings:
#: ../Sensors/Weather/__init__.py:131 ../Sensors/Weather/__init__.py:295
msgid "City:"
msgstr "Град:"

#: ../Sensors/Weather/__init__.py:131
msgid "The (English) name of your city"
msgstr "Името на града (на английски)"

#: ../Sensors/Weather/__init__.py:133
msgid "Country:"
msgstr "Държава:"

#: ../Sensors/Weather/__init__.py:133
msgid "The (English) name of your country"
msgstr "Името на държавата (на английски)"

#: ../Sensors/Weather/__init__.py:136
msgid "Update Time (Mins):"
msgstr "Обновяване на всеки (в мин.):"

#: ../Sensors/Weather/__init__.py:136
msgid "The amount of time to update the display in minutes"
msgstr "На колко минути да се обновява информацията"

#: ../Sensors/Weather/__init__.py:139
msgid "Measurement"
msgstr "Единици"

#: ../Sensors/Weather/__init__.py:140
msgid "Measurement unit:"
msgstr "Мерни единици"

#: ../Sensors/Weather/__init__.py:141
msgid "One of Imperial (UK, US) or Metric"
msgstr "Или имперски (Великобритания, САЩ), или SI"

#: ../Sensors/Weather/__init__.py:142
msgid "Metric"
msgstr "SI"

#: ../Sensors/Weather/__init__.py:143
msgid "Imperial"
msgstr "Имперски"

#: ../Sensors/Weather/__init__.py:144
msgid "Time format"
msgstr "Формат на часа"

#: ../Sensors/Weather/__init__.py:144
msgid "12 or 24 hour clock"
msgstr "12/24-часов режим"

#: ../Sensors/Weather/__init__.py:144
msgid "12 hour"
msgstr "12-часов"

#: ../Sensors/Weather/__init__.py:144
msgid "24 hour"
msgstr "24-часов"

#: ../Sensors/Weather/__init__.py:145
msgid "Advanced"
msgstr "Допълнителни"

#: ../Sensors/Weather/__init__.py:146
msgid "Direct URL:"
msgstr "Директна връзка:"

#: ../Sensors/Weather/__init__.py:146
msgid ""
"Direct link to your city's page at Yahoo.\n"
"Use this if specifying city and country above doesn't work.\n"
"Remember that you need to fill city name to have it displayed correctly."
msgstr ""
"Директна връзка към града Ви в Yahoo. Използвайте този\n"
"начин, ако указването на града и държавата не сработи. Трябва\n"
"да попълните името на града, за да се показва правилно."

#: ../Sensors/Weather/__init__.py:176
msgid "N/A"
msgstr "няма"

#: ../Sensors/Weather/__init__.py:188
msgid "calm"
msgstr "спокойно"

#: ../Sensors/Weather/__init__.py:191
msgid "mph"
msgstr "мили/ч"

#: ../Sensors/Weather/__init__.py:195
msgid "kph"
msgstr "км/ч"

#: ../Sensors/Weather/__init__.py:213
msgid "unlimited"
msgstr "неограничена"

#: ../Sensors/Weather/__init__.py:296
msgid "Temperature:"
msgstr "Температура:"

#: ../Sensors/Weather/__init__.py:297
msgid "Conditions:"
msgstr "Условия:"

#: ../Sensors/Weather/__init__.py:298
msgid "Feels like:"
msgstr "Усеща се като:"

#: ../Sensors/Weather/__init__.py:299
msgid "Dew point:"
msgstr "Точка на оросяване:"

#: ../Sensors/Weather/__init__.py:300
msgid "Pressure:"
msgstr "Налягане:"

#: ../Sensors/Weather/__init__.py:301
msgid "Pressure change:"
msgstr "Промяна на налягането:"

#: ../Sensors/Weather/__init__.py:302
msgid "Wind:"
msgstr "Вятър:"

#: ../Sensors/Weather/__init__.py:303
msgid "Humidity:"
msgstr "Влажност:"

#: ../Sensors/Weather/__init__.py:304
msgid "Sunrise:"
msgstr "Изгрев:"

#: ../Sensors/Weather/__init__.py:305
msgid "Sunset:"
msgstr "Залез:"

#: ../Sensors/Weather/__init__.py:306
msgid "Visibility:"
msgstr "Видимост:"

#: ../Sensors/Weather/__init__.py:366 ../Sensors/Weather/__init__.py:367
#: ../Sensors/Weather/__init__.py:368 ../Sensors/Weather/__init__.py:369
#: ../Sensors/Weather/__init__.py:370 ../Sensors/Weather/__init__.py:371
#: ../Sensors/Weather/__init__.py:372 ../Sensors/Weather/__init__.py:373
#: ../Sensors/Weather/__init__.py:374 ../Sensors/Weather/__init__.py:375
msgid "Retrieval Failed"
msgstr "Неуспешно получаване"
