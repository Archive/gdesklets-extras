from sensor.Sensor import Sensor

import commands
import re


#
# Sensor for watching ACPI status.
#
class ACPISensor(Sensor):

    ACPI_SOURCE = "acpi -tb"

    RE_BATTERY = re.compile("Battery[ 0-9:]+(?P<mode>[a-z]+)\, " \
                           "(?P<fill>[0-9\.]+)%(, (?P<remaining>[0-9:]+) .*)?")
    RE_THERMAL = re.compile("Thermal.*, (?P<temperature>[0-9\.]+) .*")


    def __init__(self):

        Sensor.__init__(self)
        self.add_timer(0, self.__get_data)

        

    def __get_data(self):

        fail, data = commands.getstatusoutput(self.ACPI_SOURCE)
        if (fail): self.add_timer(3000, self.__get_data)

        mode = "N/A"
        filltxt = "?"
        fill = -1
        remaining = "N/A"
        temperature = "N/A"

        lines = data.splitlines()

        for l in lines:
            if (l.count("Battery") > 0):
                mode = self.RE_BATTERY.search(l).group("mode")
                fill = int(self.RE_BATTERY.search(l).group("fill"))
                remaining = self.RE_BATTERY.search(l).group("remaining") or "N/A"
                filltxt = str(fill)
            if (l.count("Thermal") > 0):
                temperature = self.RE_THERMAL.search(l).group("temperature")
            if (l.count("AC Adapter") > 0):
                if (l.count("on-line") > 0):
                    filltxt = "online"
 
        # compute color
        if(fill == -1):
            fill = 80
            delta = 205
            if(filltxt == "online"):               
                filltxt = "online ?"
        else:
            delta = int(255.0 * (fill / 100.0))

        red = 255 - delta
        green = delta
        blue = 0
        color = "#" + hex(red + 256)[-2:] + \
                hex(green + 256)[-2:] + \
                hex(blue + 256)[-2:] + "70"

        settings = self.new_output()
        settings.set("mode", mode)
        settings.set("battery-color", color)
        settings.set("battery-fill", fill)
        settings.set("battery-fill-txt", filltxt)
        settings.set("temperature", temperature)
        settings.set("remaining", remaining)
        self.send_output(settings)

        self.add_timer(3000, self.__get_data)


def new_sensor(args): return ACPISensor( *args )
