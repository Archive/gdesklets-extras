# Copyright 2003 Jesse Andrews under GPL
# Hey Jesse, can you recognize your job ?

from sensor.Sensor import Sensor
from utils.datatypes import *

import urllib
import re
import threading
import time


class BugzillaSensor( Sensor ):


    __BUG_TYPES = (
        "UNCONFIRMED",
        "NEW",
        "ASSIGNED",
        "REOPENED",
        "NEEDINFO",
        "RESOLVED",
        "VERIFIED",
        "CLOSED"
        )



    #   <tr class="bz_normal bz_High  ">
    #
    #     <td>
    #       <a href="show_bug.cgi?id=138918">138918</a>
    #     </td>
    #
    #     <td><nobr>nor</nobr>
    #     </td>
    #     <td><nobr>Hig</nobr>
    #     </td>
    #     <td><nobr>libgtop-maint@bugzilla.gnom...</nobr>
    #     </td>
    #     <td><nobr>RESO</nobr>
    #     </td>
    #     <td><nobr>DUPL</nobr>
    #     </td>
    #     <td>libgtop doesn't compile due to missing uintmax_t
    #     </td>
    #
    #   </tr>

    __parser = re.compile(
        """<tr.*?>
        \s*?
        <td>\s*?<a.*?>(?P<id>\d+)</a>\s*?</td>
        \s*?
        <td><nobr>(?P<severity>\w*?)</nobr>\s*?</td>
        \s*?
        <td><nobr>(?P<priority>\w*?)</nobr>\s*?</td>
        \s*?
        <td><nobr>(?P<owner>.*?)</nobr>\s*?</td>
        \s*?
        <td><nobr>(?P<status>\w*?)</nobr>\s*?</td>
        \s*?
        <td><nobr>(?P<result>\w*?)</nobr>\s*?</td>
        \s*?
        <td>(?P<summary>.*?)\s*?</td>
        \s*?
        </tr>""",
        re.I | re.M | re.X
        )



    def __init__( self ):

        Sensor.__init__( self )

        ### QTY to show in the display (last qty) ###
        self._set_config_type( "qty",     TYPE_INT, 3 )
        self._set_config_type( "refresh", TYPE_INT, 10 )

        ### product/bugzilla URL ###
        self._set_config_type( "product",  TYPE_STRING, "gDesklets" )
        self._set_config_type( "bugzilla", TYPE_STRING, "http://bugzilla.gnome.org/buglist.cgi?" )

        ### what bug types to worry about ###
        self._set_config_type( "UNCONFIRMED", TYPE_BOOL, 0)
        self._set_config_type( "NEW",         TYPE_BOOL, 1)
        self._set_config_type( "ASSIGNED",    TYPE_BOOL, 1)
        self._set_config_type( "REOPENED",    TYPE_BOOL, 1)
        self._set_config_type( "NEEDINFO",    TYPE_BOOL, 0)
        self._set_config_type( "RESOLVED",    TYPE_BOOL, 0)
        self._set_config_type( "VERIFIED",    TYPE_BOOL, 0)
        self._set_config_type( "CLOSED",      TYPE_BOOL, 0)

        self.__bugs = []
        self.__checksum = -1
        self.__query = ""

        # theses events are only handled by the 2 threads (functions) :
        # __displayer and __retriever
        # theses are the only places in the sensor where events
        # are set and waited for

        self.__retriever_event = threading.Event()
        self.__displayer_event = threading.Event()

        self.watch_config( self.__on_config_changed )
        self.watch_stop( self.__on_stop )

        self.add_thread( self.__retriever )
        self.add_thread( self.__displayer )





    def __checksum(data): return len(data)
    __checksum = staticmethod(__checksum)




    def __construct_query( self ):

        """
        Creates a query for the specified bugzilla with options given.
        Returns whether the query has changed.
        """

        query  = self.get_config('bugzilla')

        query += urllib.urlencode( {'product' : self.get_config('product')} )

        # UGLY HACK!
        # we have to do this since we repeat bug_status=TYPE1&bug_status=TYPE2
        # and urlencode works from dicts, which can only have one value per key
        query += ''.join([ '&'+urllib.urlencode( {'bug_status' : bt} )
                           for bt in BugzillaSensor.__BUG_TYPES if self.get_config(bt) ]
                         )

        # maybe I should just combine CMD & product, but this is the order
        # it was created by bugzilla, so I create it in this order
        query += "&" + urllib.urlencode( { 'cmdtype'   : 'doit',
                                           'order'     : 'Bug Number',
                                           'form_name' : 'query'
                                           } )

        if self.__query != query:
            self.__query = query
            return True

        return False




    def __get_bugs( self ):

        """
        Retrieves bugs
        each bug is a dictionary containing the id, summary, ...

        @return: True if new bugs are available, else False
        @rtype: False
        """

        # maybe
        # there is a way to get the time of the last change to bugzilla
        # for a given "PRODUCT" and only query if it has changed since
        # the last time....

        try:
            data = urllib.urlopen( self.__query ).read()
            checksum = BugzillaSensor.__checksum(data)

            if checksum != self.__checksum:

                self.__bugs = [ m.groupdict()
                                for m in BugzillaSensor.__parser.finditer(data) ]

                self.__checksum = checksum
                print 'BugZilla(%s) has retrieved new bugs' % self.get_config( 'product' )
                return True

        except IOError:
            print 'BugZilla(%s) failed to retrieve bug reports' \
                  % self.get_config( 'product' )

        except:
            print 'BugZilla(%s) error' % self.get_config( 'product' )
            self.__bugs = []
            self.__checksum = -1

        print 'BugZilla(%s) nothing new' % self.get_config( 'product' )
        return False




    def __display( self ):

        print 'BugZilla(%s) refreshing' % self.get_config( 'product' )

        bugQty = len(self.__bugs)

        dispQty = min( bugQty, self.get_config( 'qty' ) )

        output = self.new_output()

        for i in range( dispQty ):
            bug = self.__bugs[bugQty-i-1]
            output.set( "bugID[%d]"       % i, bug['id']       )
            output.set( "bugSTATUS[%d]"   % i, bug['status']   )
            output.set( "bugSUMMARY[%d]"  % i, bug['summary']  )
            output.set( "bugSEVERITY[%d]" % i, bug['severity'] )
            output.set( "bugPRIORITY[%d]" % i, bug['priority'] )
            output.set( "bugOWNER[%d]"    % i, bug['owner']    )

        output.set( "qty", dispQty  )
        output.set( "product", self.get_config('product')  )
        self.send_output( output )



    def __retriever(self):

        #
        # retriever will cycle until self is stopped
        # if new bugs are available
        # the displayer is wake up
        # then sleep
        #

        self.__construct_query()

        while not self._is_stopped():

             self.__retriever_event.clear()

             if self.__get_bugs():
                 self.__displayer_event.set()

             self.__retriever_event.wait(60 * self.get_config("refresh"))



    def __displayer( self ):

        # cycle until self is stopped
        #
        # will be wake up if confiug has changed
        # or new bugs are available
        # then sleep
        #
        # on first display, self.__query may be empty
        # but that's ok


        while not self._is_stopped():

            self.__displayer_event.clear()
            
            self.__display()

            self.__displayer_event.wait()





    def __on_stop(self):
        
        self.__retriever_event.set()
        self.__displayer_event.set()




    def __on_config_changed(self, *args, **kwargs):

        # if the query has changed
        # wake up the retriever
        # else just wake up the displayer
        
        if self.__construct_query():
            self.__retriever_event.set()
            # displayer will then be wake up by the retriever
        else:
            self.__displayer_event.set()







    def call_action( self, action, path, args = [] ):
        print action, path, args



    def get_configurator( self ):
        
        conf = self._new_configurator()
        conf.set_name( "BugZilla" )
        # conf.add_title( "We query the bugzilla every 10 minutes" )
        # conf.add_title( "So, changes to configuration are not instantly shown" )

        conf.add_title( "Query" )
        conf.add_entry( "URL", "bugzilla", "Query CGI for bugzilla you want to check" )
        conf.add_entry( "Product", "product", "The product you want to query bugzilla about" )

        conf.add_title( "Bugs" )
        for bt in BugzillaSensor.__BUG_TYPES:
            conf.add_checkbox( bt.capitalize(), bt, "If enabled, query for " + bt + " bugs" )

        conf.add_spin( "Refresh Rate", "refresh", "Set the refresh rate (minutes)", 1, 120 )
        conf.add_spin( "Bugs Shown", "qty", "How many of the most recent bugs you want shown", 1, 50 )

        return conf





def new_sensor( args ): return BugzillaSensor( *args )
