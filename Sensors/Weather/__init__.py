from sensor.Sensor import Sensor
from utils.datatypes import *
from utils import i18n


import gconf

import commands
import re
import os
import time
import Queue
import urllib


def iround(f):
    return int(round(f))


#
# Sensor for retrieving weather information.
#
class WeatherSensor(Sensor):

    WEATHER_SOURCE = "http://weather.yahoo.com/search/" \
                     "weather?p=%(webcity)s,%(webcountry)s"

    # regular expressions to parse the weather data
    RE_TEMPERATURE = re.compile("\n(?P<temp>\d+)")
    RE_TEMPERATURE_HI = re.compile("")
    RE_TEMPERATURE_LO = re.compile("")
    RE_TEMPERATURE_FEEL = re.compile("<b>Feels Like:</b></font></td>\n<td><font face=Arial size=2>\n(?P<temp_feel>\d+)")
    RE_PRESSURE = re.compile("<b>Barometer:</b></font></td>\n<td><font face=Arial size=2>\n(?P<pressure>[\d\.]+) \w+ and\n(?P<pressure_change>.+)\n")
    RE_DEWPOINT = re.compile("<b>Dewpoint:</b></font></td>\n<td><font face=Arial \nsize=2> (?P<dewpoint>\d+)")
    RE_WIND = re.compile("<b>Wind:</b></font></td>\n<td><font face=Arial size=2>\n(?P<wind>[\w\s]+)")
    RE_HUMIDITY = re.compile("<b>Humidity:</b></font></td>\n<td><font face=Arial size=2>\n(?P<humidity>\d+)\%")
    RE_SUNRISE = re.compile("<b>Sunrise:</b></font></td>\n<td><font face=Arial size=2>\n(?P<sunrise>.+)</font></td>")
    RE_SUNSET = re.compile("<b>Sunset:</b></font></td>\n<td><font face=Arial size=2>\n(?P<sunset>.+)</font></td>")
    RE_VISIBILITY = re.compile("<b>Visibility:</b></font></td>\n<td><font face=Arial size=2>\n(?P<visibility>\S+)")
    RE_SKY = re.compile("--\>\n(?P<sky>[^\<]+)\</font\>\</td\>")
    RE_ICON = re.compile("\<img \nsrc=\"http://us.i1.yimg.com/us.yimg.com/"
                         "i/us/we/52/(?P<icon>\d+).gif\"\n")
    RE_MATCHES = re.compile("Weather location matches:\</font\>\</td\>\n\s*\</tr\>\</table\>\n\s*\<font face=\"arial\" size=-1\>\n\s*\<ul\>\n\s*\<li\>\n\s*\<a href=\"(?P<matchurl>[^\"]+)\"\>")

    METRIC = "metric"
    IMPERIAL = "imperial"
    AMPM = "am/pm"
    H24 = "24 hours"


    WEATHER_ICONS = {1: "rain",
                     2: "rain",
                     3: "thunder",
                     4: "thunder",
                     5: "rain",
                     6: "rain",
                     7: "ice",
                     8: "rain",
                     9: "drizzle",
                     10: "rain",
                     11: "rain",
                     12: "rain",
                     13: "snow",
                     14: "snow",
                     15: "snow",
                     16: "snow",
                     17: "thunder",
                     18: "rain",
                     19: "fog",
                     20: "fog",
                     21: "fog",
                     22: "fog",
                     23: "wind",
                     24: "wind",
                     25: "ice",
                     26: "cloud",
                     27: "luna-nori", # moon
                     28: "mostly-cloudy",
                     29: "luna-ceatza", # moon
                     30: "suncloud",
                     31: "moon",
                     32: "sun",
                     33: "moon",
                     34: "sun-light-clouds",
                     35: "thunder",
                     36: "sun",
                     37: "thunder",
                     38: "thunder",
                     39: "rain",
                     40: "rain",
                     41: "snow",
                     42: "snow",
                     43: "snow",
                     44: "suncloud",
                     45: "moon-rain", # moon
                     46: "moon-snow", # moon
                     47: "moon-tstorm" #moon
                     }



    def __init__(self):

        global _; _ = i18n.Translator("weather-sensor")
        
        self.__queue = Queue.Queue(1)
        self.__path = os.getcwd()


        Sensor.__init__(self)

        self._set_config_type("city", TYPE_STRING, "Tokyo")
        self._set_config_type("country", TYPE_STRING, "Japan")
        self._set_config_type("units", TYPE_STRING, self.METRIC)
        self._set_config_type("time", TYPE_STRING, self.H24)
        self._set_config_type("t_update", TYPE_STRING, "30")

        self.add_thread(self.__get_weather_thread)
        self.add_timer(0, self.__get_weather)



    #
    # Returns the configuration GUI.
    #
    def get_configurator(self):

        configurator = self._new_configurator()
        configurator.set_name(_("Weather"))
        configurator.add_title(_("Server Settings"))
        configurator.add_entry(_("City:"), "city", _("The (English) name "
                                                     "of your city"))
        configurator.add_entry(_("Country:"), "country", _("The (English) "
                                                           "name of your "
                                                           "country"))
        configurator.add_entry(_("Update Time (Mins):"), "t_update", _("The "
                                                    "amount of time to update "
                                                    "the display in minutes"))
        configurator.add_title(_("Measurement"))
        configurator.add_option(_("Measurement unit:"), "units",
                                _("One of Imperial (UK, US) or Metric"),
                                [(_("Metric"), self.METRIC),
                                 (_("Imperial"), self.IMPERIAL)])
        configurator.add_option(_("Time format"), "time", _("12 or 24 hour clock"), [(_("12 hour"), self.AMPM), (_("24 hour"), self.H24)])
        configurator.add_title(_("Advanced"))
        configurator.add_entry(_("Direct URL:"), "url", _("Direct link to your city's page at Yahoo.\nUse this if specifying city and country above doesn't work.\nRemember that you need to fill city name to have it displayed correctly."))
        
        return configurator



    def __get_http_proxy(self):
        proxy  = None
        client = gconf.client_get_default()
        value  = client.get("/system/http_proxy/use_http_proxy")
        active = value.get_bool()
        if (active):
            v = client.get("/system/http_proxy/host")
            if (not v):
                proxy = v.get_string()
        return proxy

    def __get_temp(self, value):
        v = float(value)
        if (self.units == self.METRIC):
            v = (v - 32) * (5 / 9.0)
        return iround(v)

    def __get_pressure(self, value):
        try:
            val = float(value)
            if (self.units == self.METRIC):
                val *= 33.8639 # hecto pascals
            return "%.2f" % (val,)
        except:
            return _('N/A')

    def __get_pressure_change(self, value):
        return value
        chg = value.split(' ')
        if (len(chg) > 1):
            return _(chg[3])
        return _(chg[1])

    def __get_wind(self, value):
        tmp = value.split(' ')
        if (len(tmp) == 1):
            return _('calm')
        
        val = float(tmp[1])
        uni = _('mph')
        dir = tmp[0]
        if (self.units == self.METRIC):
            val = iround(val * 1.60934)
            uni = _('kph')
        return "%d %s %s" % (val, _(uni), _(dir))

    def __get_time(self, value):
        t = value
        if(self.time == self.H24):
            t = t.replace(':', ' ')
            t = t.split(' ')
            m = int(t[1])
            h = int(t[0])
            if(t[2] == 'pm'):
                h = (h + 12) % 24
            t = "%02d:%02d" % (h, m)
        return t

    def __get_visibility(self, value):
        tmp = value.split(' ')
        if (len(tmp) == 1):
            return _('unlimited')
        
        val = tmp[0]
        uni = tmp[1]
        if (self.units == self.METRIC):
            val = iround(val * 1.60934)
            
        return "%d $s" % (val, _(uni))
        

    #
    # Retrieves the weather data.
    #
    def __get_weather(self):

        # check the queue regularly
        if (not self.__queue.empty()):
            output = self.__queue.get()
            self.send_output(output)

        self.add_timer(500, self.__get_weather)
        



    #
    # Thread function for retrieving the weather data since this may take time
    # and must not block the application.
    #
    def __get_weather_thread(self):

        counter = 0
        old_values = ("", "", "", "", "")
        current_time = int(time.time())
        http_proxy = self.__get_http_proxy()
        # Set us back t_update seconds so the if statment will be true
        t_update = int(self.get_config("t_update"))
        last_update = current_time - (t_update * 60) - 1
        
        while (1):
            if (self.is_stopped()): break

            city = self.get_config("city")
            country = self.get_config("country")
            self.units = self.get_config("units")
            self.time = self.get_config("time")
            t_update = int(self.get_config("t_update"))
            url = self.get_config("url")
            webcity = urllib.quote(city)
            webcountry = urllib.quote(country)

            fixed_url = url.strip()
            if(fixed_url != ""):
                if(fixed_url.find("http://", 0, 7) == -1):
                    fixed_url = "http://" + fixed_url
                fixed_url = urllib.quote(fixed_url, ":/") # don't quote : in 'http://'
            else:
                fixed_url = self.WEATHER_SOURCE % vars()

            # Initalize the current time. 
            current_time = int(time.time())


            if ((city, country, self.units, self.time, t_update, url) != old_values or \
                last_update + (t_update * 60) <= current_time):
                # At least 300/60 = 5 minutes this will run. Or so I thought...
                # Sleep can take a few seconds to many minutes, it's not
                # accurate at all.  This is a problem for me, I'm picky.  CN
                old_values = (city, country, self.units, self.time, t_update, url)
                current_time = int(time.time())
                last_update = int(time.time())

                if (self.units == self.METRIC):
                    unit_temp = "&#176;C"
                    unit_pres = "hPa"
                else:
                    unit_temp = "&#176;F"
                    unit_pres = "in"

                output = self.new_output()

                # translateable strings:
                output.set("label_loca", _("City:"))
                output.set("label_temp", _("Temperature:"))
                output.set("label_cond", _("Conditions:"))
                output.set("label_feel", _("Feels like:"))
                output.set("label_dewp", _("Dew point:"))
                output.set("label_pres", _("Pressure:"))
                output.set("label_prch", _("Pressure change:"))
                output.set("label_wind", _("Wind:"))
                output.set("label_humi", _("Humidity:"))
                output.set("label_sunr", _("Sunrise:"))
                output.set("label_suns", _("Sunset:"))
                output.set("label_visi", _("Visibility:"))
                
                
                try:
                    fh = urllib.urlopen(fixed_url, http_proxy)
                    data = fh.read()

                    # Take first match if we have ambiguity
                    if (data.find("Weather location matches") != -1):
                        output.set("location", city + " [ambiguous]")

                        # URL of the page listing the location matches
                        matches_url = fh.geturl()
                        match_href = self.RE_MATCHES.search(data).group("matchurl")
                        if (match_href.find("http://", 0, 7) != -1):
                            # href = complete url
                            match_url = match_href
                        elif (match_href.find("/", 0, 1) != -1):
                            # href = absolute address
                            match_url = re.compile("(?P<host>http://([a-zA-Z\-\_.?]+))/").search(matches_url).group("host") + match_href
                        else:
                            # href = relative address
                            match_url = re.compile("(?P<urlbase>http://[a-zA-Z\-\_.?]+/(.*/)*)(.*)").search(matches_url).group("urlbase") + match_href
                        # Open the URL of the first match and search it for weather data
                        fh = urllib.urlopen(match_url, http_proxy)
                        data = fh.read()

                    output.set("location", city)

                    temperature = self.__get_temp(self.RE_TEMPERATURE.search(data).group("temp"))
                    temperature_feel = self.__get_temp(self.RE_TEMPERATURE_FEEL.search(data).group("temp_feel"))
                    dewpoint = self.__get_temp(self.RE_DEWPOINT.search(data).group("dewpoint"))
                    pressure = self.__get_pressure(self.RE_PRESSURE.search(data).group("pressure"))
                    pressure_change = self.__get_pressure_change(self.RE_PRESSURE.search(data).group("pressure_change"))
                    wind = self.__get_wind(self.RE_WIND.search(data).group("wind"))
                    humidity = '%i%%' % int(self.RE_HUMIDITY.search(data).group("humidity"))
                    sunrise = self.__get_time(self.RE_SUNRISE.search(data).group("sunrise"))
                    sunset = self.__get_time(self.RE_SUNSET.search(data).group("sunset"))
                    visibility = self.__get_visibility(self.RE_VISIBILITY.search(data).group("visibility"))
                    sky = self.RE_SKY.search(data).group("sky")
                    icon_nr = self.RE_ICON.search(data).group("icon")
                    icon = self.WEATHER_ICONS.get(int(icon_nr), 30)

                    output.set("temperature", temperature)
                    output.set("temperature_feel", temperature_feel)
                    output.set("dewpoint", dewpoint)
                    output.set("pressure", pressure)
                    output.set("pressure_change", pressure_change)
                    output.set("wind", wind)
                    output.set("humidity", humidity)
                    output.set("sunrise", sunrise)
                    output.set("sunset", sunset)
                    output.set("visibility", visibility)
                    output.set("sky", sky)
                    output.set("icon", os.path.join(self.__path, "icons",
                                                "%(icon)s.png" % vars()))
                    output.set("action", "exec(gnome-open \"%s\")" % fixed_url) # action to launch browser

                except Exception, e:
                    print e
                    output.set("temperature", _("Retrieval Failed"))
                    output.set("temperature_feel", _("Retrieval Failed"))
                    output.set("dewpoint", _("Retrieval Failed"))
                    output.set("pressure", _("Retrieval Failed"))
                    output.set("pressure_change", _("Retrieval Failed"))
                    output.set("wind", _("Retrieval Failed"))
                    output.set("humidity", _("Retrieval Failed"))
                    output.set("sunrise", _("Retrieval Failed"))
                    output.set("sunset", _("Retrieval Failed"))
                    output.set("visibility", _("Retrieval Failed"))
                    output.set("sky", "N/A")
                    output.set("icon", os.path.join(self.__path,
                                                    "icons/unknown.png"))
                    output.set("url", "")
                    unit_temp = ''
                    unit_pres = ''
 
                output.set("unit_temp", unit_temp)
                output.set("unit_pres", unit_pres)
                self.__queue.put(output)
            #end if
            time.sleep(1)
        #end while
            


print """
The weather sensor uses data from weather.yahoo.com.
Yahoo! claims:

************************** NOTICE *******************************
The data (conditions, forecasts, news, images, logos) contained in
this page are copyrighted by Yahoo! Inc. and the Weather Channel
Enterprises, Inc. You are prohibited from using or repurposing this
data in any way without express written consent from Yahoo! Inc. and
the Weather Channel Enterprises, Inc.
If you are looking for a source of weather data, please see the
National Weather Service Website at http://www.nws.noaa.gov/. Their
data is public information, to be used with appropriate
byline/photo/image credits.
************************** NOTICE *******************************
"""


def new_sensor(args): return WeatherSensor( *args )
