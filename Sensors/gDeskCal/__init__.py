from sensor.Sensor import Sensor

import calendar
import time


class gDeskCal(Sensor):

    def __init__(self):

        Sensor.__init__(self)

        self.add_timer(1000, self.__make_calendar)



    #
    # Returns the current day, month and year.
    #
    def __get_current_day(self):

        now = time.localtime(time.time())
        year = now[0]
        month = now[1]
        day = now[2]

        return (day, month, year)




    def __make_calendar(self):

        day, month, year = self.__get_current_day()
        cal = calendar.month(year, month)
        lines = cal.splitlines()

        monthtext, yeartext = lines[0].split()[:2]
        weekdays = lines[1].split()
        days = "\n".join(lines[2:])

        settings = self.new_output()
        settings.set("month", monthtext)
        settings.set("year", yeartext)

        for i in xrange(7):
            value = weekdays[i]
            settings.set("weekday[" + str(i) + "]", value)

        for i in xrange(42):
            value = days[i * 3:i * 3 + 2]
            if (value.strip().isdigit()):
                day = int(value)
            else:
                day = 0

            settings.set("day[" + str(i) + "]", value)

        self.send_output(settings)

        


def new_sensor(args): return gDeskCal( *args )
