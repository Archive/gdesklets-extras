from sensor.Sensor import Sensor
from utils.datatypes import *
from utils import i18n

import time
import math
import os


#
# Sensor for a clock.
#
# Blatantly modified by Tuomas Kuosmanen <tigert@ximian.com> to use a nifty
# clock face I "found from the internet" :^) - basically a test on how to 
# customize these, and it looked nice so I thought I'd share.
#
class ClockSensor(Sensor):

    # the default clock hands
    HAND_HOUR = [-0.03, -0.2,
                 0.03, -0.2,
                 0.03, 0.6,
                 -0.03, 0.6]

    HAND_MINUTE = [-0.03, -0.2,
                   0.03, -0.2,
                   0.03, 0.9,
                   -0.03, 0.9]

    HAND_SECOND = [-0.02, -0.2,
                   0.02, -0.2,
                   0.02, 1.0,
                   -0.02, 1.0]

    MODE_12H = "12h"
    MODE_24H = "24h"


    def __init__(self, hand_hour = "", hand_minute = "", hand_second = ""):

        global _; _ = i18n.Translator("clock-sensor")

        self.__hand_hour = hand_hour or self.HAND_HOUR
        self.__hand_minute = hand_minute or self.HAND_MINUTE
        self.__hand_second = hand_hour or self.HAND_SECOND

        self.__timezones = [(_("local"), "localtime"),
                            (_("UTC"), "UTC0")]


        Sensor.__init__(self)
        self._set_config_type("show-seconds", TYPE_BOOL, 1)
        self._set_config_type("show-date", TYPE_BOOL, 1)
        self._set_config_type("mode", TYPE_STRING, self.MODE_24H)
        self._set_config_type("timezone", TYPE_STRING, "localtime")

        self.__timezones += self.__load_timezones()
        
        self.add_timer(0, self.__get_data)



    #
    # Loads the timezones and returns tuples (name, tzname).
    #
    def __load_timezones(self):

        zones = []
        try:
            fd = open("timezones.dat", "r")
            try:
                lines = fd.readlines()
            finally:
                fd.close()
            for l in lines:
                name = l.strip()
                zones.append((name, name))
            #end for
        except:
            pass

        return zones



    #
    # Returns the configuration GUI.
    #
    # TODO: add configuration option for UTC time
    #
    def get_configurator(self):

        configurator = self._new_configurator()
        configurator.set_name(_("Clock"))
        configurator.add_title(_("Clock Settings"))
        configurator.add_checkbox(_("_Show seconds"), "show-seconds",
                              _("Toggles the seconds on and off"))
        configurator.add_checkbox(_("Show _date"), "show-date",
                              _("Toggles the date on and off"))
        configurator.add_option(_("Display mode (digital clock)"), "mode",
                                _("12h or 24h display mode"),
                                [(_("12h"), self.MODE_12H),
                                 (_("24h"), self.MODE_24H)])
        
        configurator.add_title(_("Regional Settings"))
        configurator.add_option(_("Timezone"), "timezone",
                                _("The timezone to use"),
                                self.__timezones)
        
        return configurator



    #
    # Rotates the given point by the given angle.
    #
    def __rotate(self, x, y, angle):

        cos_a = math.cos(angle)
        sin_a = math.sin(angle)
        rx = x * cos_a - y * sin_a
        ry = y * cos_a + x * sin_a

        return (rx, ry)



    #
    # Generates and returns the drawing instructions for a clock hand.
    #
    def __do_hand(self, points, angle):

        p = ""
        for i in xrange(0, len(points), 2):
            x = points[i]
            y = points[i + 1]
            rx, ry = self.__rotate(x, y, angle)
            p += " %(rx)f %(ry)f" % vars()
        #end for

        return p



    def __get_data(self):

        timezone = self.get_config("timezone")
        show_secs = self.get_config("show-seconds")
        show_date = self.get_config("show-date")
        mode = self.get_config("mode")

        # get the current time for the selected timezone
        tzname = time.tzname[1] or time.tzname[0]
        os.environ["TZ"] = timezone
        current_time = time.localtime(time.time())
        os.environ["TZ"] = tzname

        hour = current_time[3]
        minute = current_time[4]
        second = current_time[5]

        h_angle = -(2 * math.pi) / 12 * (hour + (minute / 60.0))
        m_angle = -(2 * math.pi) / 60 * minute
        s_angle = -(2 * math.pi) / 60 * second

        gfx_hour = self.__do_hand(self.__hand_hour, h_angle)
        gfx_minute = self.__do_hand(self.__hand_minute, m_angle)
        if (show_secs):
            gfx_second = self.__do_hand(self.__hand_second, s_angle)
        else:
            gfx_second = ""


        if (mode == self.MODE_12H and hour > 12):
            hour -= 12
            daytime = "pm"
        else:
            daytime = "am"


        signal = self.new_output()
        signal.set("date", self.__get_date(current_time))

        # digital clock
        signal.set("daytime", daytime)
        signal.set("hours", "%(hour)02d" % vars())
        signal.set("minutes", "%(minute)02d" % vars())
        
        if (show_secs):
            signal.set("seconds", "%(second)02d" % vars())
        else:
            signal.set("seconds", "")
            
        if (show_date):
            signal.set("date", self.__get_date(current_time))
        else:
            signal.set("date", "")

            
        # analogue clock
        signal.set("hands", "clear, color black, "
                            "polygon %(gfx_hour)s 1, "
                            "polygon %(gfx_minute)s 1,"
                            "color red,"
                            "polygon %(gfx_second)s 1" % vars())

        self.send_output(signal)

        self.add_timer(1000, self.__get_data)



    def __get_date(self, current_time):

        date = time.strftime("%x", current_time)
        return date


def new_sensor(args): return ClockSensor( *args )
