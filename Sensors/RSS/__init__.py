from sensor.Sensor import Sensor
from utils.datatypes import *
from utils import i18n

import commands
import urllib
import os
import feedparser
import re
import string
import time

#=====================================================================
# Implements an RSS reader
# Inspired by and (initially) based on rss-0.1 by dweazle
# Dave Masterson, Dec 2003
#
# Many parts added by gsergiu, Jan 2004, related to clickable 
# headlines - many thanks :-)
#=====================================================================
class RssSensor(Sensor):

    # Setup the Sensor
    def __init__(self):

        global _; _ = i18n.Translator("rss-sensor")
        Sensor.__init__(self)

        # create vars
        self.__maintitle = "Connecting\n(updated @ 00:00)"
        self.__lutime = "00:00"

        self.__titles = []
        self.__links = []
 
        # Default values for config
        self._set_config_type("url", TYPE_STRING,
                              "http://gdesklets.gnomedesktop.org/rss.php")
        self._set_config_type("update", TYPE_INT, 30)
        self._set_config_type("font", TYPE_STRING, "Arial 12")
        self._set_config_type("fontcolor", TYPE_STRING, "#00FF00")
        self._set_config_type("titlefont", TYPE_STRING, "Arial 14")
        self._set_config_type("titlefontcolor", TYPE_STRING, "#FFFF00")
        self._set_config_type("highlightfont", TYPE_STRING, "Arial 14")
        self._set_config_type("highlightfontcolor", TYPE_STRING, "#FFFFFF")

        # go grab the feed
        self.add_thread(self.__get_news_thread)
        self.add_timer(5000, self.__display_feed)



    def __get_news_thread(self):
                
        while (1):

            if (self.is_stopped()): break
            
            # get the config values         
            url      = self.get_config("url")
            myupdate = int(self.get_config("update")) * 60

            # Call the parser to populate rssdata
            rssdata = feedparser.parse(url)

            # Get the channel name
            self.__maintitle = "%s\n(updated @ %s)" % \
                               (rssdata['channel']['title'], time.strftime("%H:%M"))

            # Clear the arrays
            # self.__titles [0:] = []
            # self.__links  [0:] = []

            # get the headlines     
            for item in rssdata['items']:
                wtitle = ""
                title = item['title']

                # add link and titles to appropriate arrays
                self.__titles.append(wtitle[:-1])
                self.__links.append(item['link'])

            # wait to go again
            time.sleep(myupdate)



    # Get the RSS feed, parse it and send it to the display
    def __display_feed(self):

        # Send the title to the display
        output = self.new_output()
        output.set("name", self.__maintitle)
        output.set("namefont", self.get_config("titlefont"))
        output.set("namecolor", self.get_config("titlefontcolor"))
        
        # iterate through headlines and display them... 
        index = 0
        for headline in self.__titles:
            output.set("item_value[%(index)d]" % vars(), self.__titles[index])
            output.set("font[%(index)d]" % vars(), self.get_config("font"))
            output.set("color[%(index)d]" % vars(), self.get_config("fontcolor"))
            index += 1

        self.send_output(output)
 
        # ready to do it all over again
        self.add_timer(10000, self.__display_feed)



    # override this method to enable user interaction
    def call_action(self, action, path, args = []):

        index = int(path[-1])
        if (action == "launch"):
            try:
                # open the link in the default gnome browser
                import gnome
                gnome.url_show(self.__links[index])
            except:
                print "could not open", uri

        elif (action == "leave"):
            output = self.new_output()
            output.set("item_value[%(index)d]" % vars(), self.__titles[index])
            output.set("font[%(index)d]" % vars(), self.get_config("font"))
            output.set("color[%(index)d]" % vars(), self.get_config("fontcolor"))
            self.send_output(output)

        elif (action == "enter"):
            output = self.new_output()
            output.set("item_value[%(index)d]" % vars(), self.__titles[index])
            output.set("font[%(index)d]" % vars(),
                       self.get_config("highlightfont"))
            output.set("color[%(index)d]" % vars(),
                       self.get_config("highlightfontcolor"))
            self.send_output(output)



    # Add extra config fields
    def get_configurator(self):

        conf1 = self._new_configurator()
        conf1.set_name(_("RSS"))
        conf1.add_title(_("RSS Source URL"))
        conf1.add_entry(_("URL",), "url", _("The URL of the RSS feed"))
        conf1.add_title(_("Interval between RSS updates (minutes)"))
        conf1.add_spin(_("Interval",), "update",
                       _("The interval between updates (minutes)"), 1, 60)
        conf1.add_title(_("Font Settings"))
        conf1.add_font_selector(_("List font:"), "font",
                                _("Font used in the URL list"))
        conf1.add_color_selector(_("List font color:"), "fontcolor",
                                 _("Color used in the URL list"))
        conf1.add_font_selector(_("Title font:"), "titlefont",
                                _("Font used in the title"))
        conf1.add_color_selector(_("Title font color:"), "titlefontcolor",
                                 _("Color used in the title"))
        conf1.add_font_selector(_("Highlight font:"), "highlightfont",
                                _("Font used to highlight link"))
        conf1.add_color_selector(_("Highlight font color:"),
                                 "highlightfontcolor",
                                 _("Color used to highlight link"))

        return conf1

# Advertise the sensor
def new_sensor(args): return RssSensor( *args )
