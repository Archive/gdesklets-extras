from sensor.Sensor import Sensor

import gtk
import os
import math


# the path where to find all icons
ICONPATH = os.path.expanduser("~/dock")


#
# Sensors have to inherit from sensor.Sensor.
#
class IconBar(Sensor):

    def __init__(self):

        self.__icons = []
        self.__path = os.getcwd()
        self.__zoom_level = []
        self.__zoom_to = []
        self.__align = []
        self.__align_to = []
        self.__is_zooming = 0

        self.__bouncing_index = -1

        self.__current_index = 0


        Sensor.__init__(self)
        self.add_timer(0, self.__init_icons)




    def __read_desktop_file(self, filename):

        info = {}
        fd = open(filename, "r")
        lines = fd.readlines()
        fd.close()
        for l in lines:
            if (not l): continue
            elif (l[0] == "["): continue
            else:
                index = l.find("=")
                key = l[:index]
                value = l[index + 1:]
                info[key] = value[:-1]
        #end for

        return info



    def __init_icons(self):

        self.__icons = []
        settings = self.new_output()
        files = os.listdir(ICONPATH)
        cnt = 0
        for f in files:
            path = os.path.join(ICONPATH, f)
            if (os.path.isfile(path)):
                info = self.__read_desktop_file(path)
                if (not info.has_key("Exec")): continue
                uri = info["Icon"]
                action = info["Exec"]
                self.__icons.append((uri, action))

                self.__zoom_level.append(1.0)
                self.__zoom_to.append(1.0)
                self.__align.append(0.0)
                self.__align_to.append(0.0)
                path = os.path.join("/usr/share/pixmaps", uri)
                settings.set("icon-uri[" + str(cnt) + "]", str(path))
                cnt += 1
            #end if
        #end for

        self.send_output(settings)
        self.add_timer(250, self.__check_running_apps)




    def __zoom_function(self, x):

        a = -0.45
        b = 1.6

        return max(1.0, a * x*x + b)



    def __align_function(self, x):

        a = 0.1
        b = 0.0
        c = 0.0

        return max(-0.5, min(0.5, 1.0 * x)) #a * x*x + b*x + c))




    def __zoom_handler(self):

        self.__is_zooming = 1

        while (gtk.events_pending()): gtk.mainiteration()

        settings = self.new_output()

        busy = 0
        for i in xrange(len(self.__zoom_to)):
            busy |= self.__zoom_icon(i)
            busy |= self.__align_icon(i)
            settings.set("icon-scale[" + str(i) + "]", self.__zoom_level[i])
            settings.set("icon-align[" + str(i) + "]", 0.5 + self.__align[i])
        #end for

        self.send_output(settings)

        if (not busy):
            self.__is_zooming = 0
            return gtk.FALSE

        # this is experimental to lower CPU load
        #import time
        #time.sleep(0.02)
        
        return gtk.TRUE



    def __zoom_icon(self, index):

        zoom_to = self.__zoom_to[index]
        zoom = self.__zoom_level[index]

        if (abs(zoom - zoom_to) < 0.005):
            if (zoom > 1.6):
                settings = self.new_output()
                settings.set("visible[" + str(index) + "]", "1")
                self.send_output(settings)
            return 0
        else:
            sgn = (zoom < zoom_to) and 1 or -1
            # make sure all zoom actions are completed at the same time
            delta = (zoom_to - zoom) / 3.0
            self.__zoom_level[index] += delta
            self.__zoom_level[index] = max(0.1, self.__zoom_level[index])

            return 1



    def __align_icon(self, index):

        align_to = self.__align_to[index]
        align = self.__align[index]

        if (abs(align - align_to) < 0.005):
            return 0
        else:
            sgn = (align < align_to) and 1 or -1
            # make sure all align actions are completed together with the zoom
            # actions
            delta = (align_to - align) / 3.0
            self.__align[index] += delta

            return 1



    def __bounce_function(self, x):

        return abs(math.cos(0.9 * x))



    def __bounce_icon(self, index, x):

        align = self.__bounce_function(x)
        settings = self.new_output()
        settings.set("icon-jump[" + str(index) + "]", align)
        self.send_output(settings)
        x += 0.2

        if (x <= 3.4 * math.pi):
            self.__bouncing_index = index
            gtk.timeout_add(50, self.__bounce_icon, index, x)
        else:
            self.__bouncing_index = -1



    def __hilight_icon(self, index):

        settings = self.new_output()

        f = self.__zoom_function
        g = self.__align_function
        for i in xrange(len(self.__zoom_to)):
            if (i != index):
                settings.set("visible[" + str(i) + "]", "0")
            self.__zoom_to[i] = f(i - index)
            self.__align_to[i] = g(i - index)

        if (not self.__is_zooming):
            gtk.timeout_add(30, self.__zoom_handler)

        uri, action = self.__icons[index]
        settings.set("description[" + str(index) + "]", action)
        self.send_output(settings)



    def __unhilight_icon(self, index):

        settings = self.new_output()
        for i in xrange(len(self.__zoom_to)):
            settings.set("visible[" + str(i) + "]", "0")
            self.__zoom_to[i] = 1.0
            self.__align_to[i] = 0.0

        if (not self.__is_zooming):
            gtk.timeout_add(30, self.__zoom_handler)

        self.send_output(settings)



    #
    # Checks whether some applications are running.
    #
    def __check_running_apps(self):

        import commands
        output = self.new_output()
        i = self.__current_index
        uri, action = self.__icons[i]
        fail, processes = commands.getstatusoutput("ps -A --format='%a'")
        if (processes.find(action) != -1):
            output.set("is-running[" + str(i) + "]", "true")
        else:
            output.set("is-running[" + str(i) + "]", "false")
        
        self.send_output(output)

        self.__current_index += 1
        if (self.__current_index == len(self.__icons)):
            self.__current_index = 0

        return gtk.TRUE
            


    def call_action(self, action, path, args = []):

        if (action == "enter"):
            index = int(path[-1])
            if (index != self.__bouncing_index): self.__hilight_icon(index)

        elif (action == "leave"):
            index = 0#int(path[1])
            self.__unhilight_icon(index)

        elif (action == "launch"):
            index = int(path[-1])
            self.__unhilight_icon(index)
            gtk.timeout_add(0, self.__bounce_icon, index, 0)

            uri, action = self.__icons[index]
            os.system(action + " &")



def new_sensor(args): return IconBar( *args )
