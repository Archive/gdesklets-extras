from sensor.Sensor import Sensor

import xmms


class XmmsSensor( Sensor ):

	def __init__( self ):
		Sensor.__init__( self )
		self.add_timer( 0, self.__get_data )

	def __get_data( self ):
		try:
			cur = xmms.get_playlist_pos()
			title = xmms.get_playlist_title( cur )
			time = xmms.get_output_time() 
			length = xmms.get_playlist_time( cur )
			fill = float(time) / float(length)

		except:
			title, length, time, fill = "XMMS Not Loaded", 0, 0, 0

		output = self.new_output()
		output.set( "title", title )
		output.set( "length", "%d:%02d" % (length/60000,(length/1000)%60))
		output.set( "curtime", "%d:%02d" % (time/60000,(time/1000)%60))
		output.set( "fill", int(fill*100) )

		self.add_timer( 1000, self.__get_data )
		self.send_output( output )

	def call_action( self, action, path, args = []):
		
		if (action == "stop"):
			xmms.stop()

		if (action == "play"):
			xmms.play()

		if (action == "next"):
			xmms.playlist_next()

		if (action == "prev"):
			xmms.playlist_prev()

		if (action == "pause"):
			xmms.pause()


def new_sensor( args ): return XmmsSensor( *args )
