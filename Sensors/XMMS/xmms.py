# Released by Michael Hudson on 2000-07-01, and again on 2001-04-26
# public domain; no warranty, no restrictions

import struct
from xmmscmds import *
import xmmsint
    
def pack_file_name(file):
    padding = "\000" * (((len(file) + 1 + 3) / 4) * 4 - len(file))
    prefix = struct.pack("l",len(file) + 1)
    return prefix + file + padding

def get_string_pos(cmd,pos):
    return xmmsint.send_request_args(cmd,"l",pos)[:-1]

def get_version():
    return xmmsint.send_requestf(CMD_GET_VERSION,"l")[0]
def playlist_add(files):
    buf = []
    if type(files) == type(""):
        files = [files]
    for file in files:
        buf.append(pack_file_name(file))
    return xmmsint.send_command(CMD_PLAYLIST_ADD,
                                "".join(buf)+"\000\000\000\000")
def play(): xmmsint.send_command(CMD_PLAY)
def pause(): xmmsint.send_command(CMD_PAUSE)
def stop(): xmmsint.send_command(CMD_STOP)
def eject(): xmmsint.send_command(CMD_EJECT)
def is_playing(): return xmmsint.send_requestf(CMD_IS_PLAYING,'l')[0]
def is_paused(): return xmmsint.send_requestf(CMD_IS_PAUSED,'l')[0]
def get_playlist_pos():
    return xmmsint.send_requestf(CMD_GET_PLAYLIST_POS,'l')[0]
def get_playlist_file(pos):
    return xmmsint.send_request_args(CMD_GET_PLAYLIST_FILE,'l',pos)[:-1]
def get_playlist_title(pos):
    return xmmsint.send_request_args(CMD_GET_PLAYLIST_TITLE,'l',pos)[:-1]
def get_playlist_time(pos):
    return xmmsint.send_requestf_args(CMD_GET_PLAYLIST_TIME,'l','l',pos)[0]
def get_info():
    """returns (bitrate, freq, nch)"""
    return xmmsint.send_requestf_args(CMD_GET_INFO, 'lll')

def set_playlist_pos(pos):
    xmmsint.send_command_args(CMD_SET_PLAYLIST_POS,'l',pos)
def get_playlist_length():
    return xmmsint.send_requestf(CMD_GET_PLAYLIST_LENGTH,'l')[0]
def playlist_clear(): xmmsint.send_command(CMD_PLAYLIST_CLEAR)
def get_output_time(): # in milliseconds!
    return xmmsint.send_requestf(CMD_GET_OUTPUT_TIME,'l')[0]
def jump_to_time(time):
    xmmsint.send_command_args(CMD_JUMP_TO_TIME,'l',time)
def get_volume():
    return xmmsint.send_requestf(CMD_GET_VOLUME,"ll")
def get_balance():
    return xmmsint.send_requestf(CMD_GET_BALANCE,"l")
def set_volume(left,right=None):
    if right is None:
        right = left
    xmmsint.send_command_args(CMD_SET_VOLUME,"ll",left,right)
def get_skin():
    return xmmsint.send_request(CMD_GET_SKIN)[:-1]

def playlist_next(): xmmsint.send_command(CMD_PLAYLIST_NEXT)
def playlist_prev(): xmmsint.send_command(CMD_PLAYLIST_PREV)
def toggle_shuffle(): xmmsint.send_command(CMD_TOGGLE_SHUFFLE)
def toggle_repeat(): xmmsint.send_command(CMD_TOGGLE_REPEAT)

def is_main_win():
    return xmmsint.send_requestf(CMD_IS_MAIN_WIN,'l')[0]
def is_pl_win():
    return xmmsint.send_requestf(CMD_IS_PL_WIN,'l')[0]
def is_eq_win():
    return xmmsint.send_requestf(CMD_IS_EQ_WIN,'l')[0]

def set_main_win_vis(vis):
    xmmsint.send_command_args(CMD_MAIN_WIN_TOGGLE, "i", not not vis)
def set_pl_win_vis(vis):
    xmmsint.send_command_args(CMD_MAIN_WIN_TOGGLE, "i", not not vis)
def set_eq_win_vis(vis):
    xmmsint.send_command_args(CMD_MAIN_WIN_TOGGLE, "i", not not vis)
def set_always_on_top(vis):
    xmmsint.send_command_args(CMD_TOGGLE_AOT, "i", not not vis)

def show_prefs_box():
    xmmsint.send_command(CMD_SHOW_PREFS_BOX)
def show_about_box():
    xmmsint.send_command(CMD_SHOW_ABOUT_BOX)

def fade(delay=10,start=100):
    import time
    for i in range(start):
        set_volume(start-i)
        time.sleep(delay)
    stop()

if __name__=='__main__':
    import sys
    fade(int(sys.argv[1]))
