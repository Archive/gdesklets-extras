# Released by Michael Hudson on 2000-07-01, and again on 2001-04-26
# public domain; no warranty, no restrictions

import socket,struct,os,pwd

class ClientPacketHeader:
    def __init__(self):
        self.version,self.cmd,self.length = 0,0,0
    def __repr__(self):
        return "<< %s : version: %s cmd: %s length: %s >>"\
               %(self.__class__.__name__,self.version,self.cmd,self.length)
    def encode(self):
        return struct.pack("hhl",self.version,self.cmd,self.length)

def read_header(sock):
    head = ClientPacketHeader()
    head.version, head.cmd, head.length = \
                  struct.unpack("hhl",sock.recv(8))
    return head

class XmmsConnection:
    def __init__(self,session=0):
        self.sock = get_socket(session)
    def send(self,cmd,data=""):
        if type(data) == type(1):
            data = struct.pack("l",data)
        packet = struct.pack("hhl",1,cmd,len(data))+data
        self.sock.send(packet)
    def sendf(self,cmd,fmt,args):
        data = struct.pack( * [fmt]+args )
        self.send(cmd,data)
    def get_reply(self):
        self.reply_header = read_header(self.sock)
        return self.sock.recv(self.reply_header.length)
    def get_replyf(self,fmt):
        return struct.unpack(fmt,self.get_reply())
    def read_ack(self):
        self.get_reply()
        
def get_socket(sess):
    sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
    sock.connect("/tmp/xmms_%s.%d"%(pwd.getpwuid(os.geteuid())[0],sess))
    return sock

def send_command(cmd,data=""):
    conn = XmmsConnection()
    conn.send(cmd,data)
    conn.read_ack()
def send_command_args(cmd,fmt,*args):
    conn = XmmsConnection()
    conn.sendf(cmd,fmt,args)
    conn.read_ack()
def send_request(cmd,data=""):
    conn = XmmsConnection()
    conn.send(cmd,data)
    return conn.get_reply()
def send_request_args(cmd,fmt,*args):
    conn = XmmsConnection()
    conn.sendf(cmd,fmt,args)
    return conn.get_reply()
def send_requestf(cmd,fmt,data=""):
    conn = XmmsConnection()
    conn.send(cmd,data)
    return conn.get_replyf(fmt)    
def send_requestf_args(cmd,rfmt,fmt,*args):
    conn = XmmsConnection()
    conn.sendf(cmd,fmt,args)
    return conn.get_replyf(rfmt)
