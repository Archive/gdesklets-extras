import libdesklets as lib

class SysInfo:

    def __init__(self, main):

        self.__main  = main
        self.__parts = []
        self.__net   = []



    def get_partitions(self):

        for dev, mpoint in lib.disk.get_partitions():
            self.__parts.append([dev, mpoint])

        return self.__parts



    def get_network(self):

        for dev in lib.net.get_devices():
            self.__net.append([dev, dev])

        return self.__net



    def set_sys(self, signal, index):

        os                             = lib.sys.get_os()
        arch                           = lib.sys.get_arch()
        name                           = lib.sys.get_name()
        kernel                         = lib.sys.get_kernel()
        l1                             = lib.sys.get_load_avg_1m()
        l5                             = lib.sys.get_load_avg_5m()
        l15                            = lib.sys.get_load_avg_15m()
        tasks, running                 = lib.sys.get_tasks()
        users                          = lib.sys.get_users()
        uptime                         = lib.sys.get_uptime()
        d, h, m, s                     = lib.convert.secs_to_dhms(uptime)
        idletime                       = lib.sys.get_idletime()
        idle_d, idle_h, idle_m, idle_s = lib.convert.secs_to_dhms(idletime)
        idle_p                         = (idletime / float(uptime)) * 100

        signal.set("value1[%(index)d]"    % vars(), os + " @ " + \
                   name.split(".")[0])
        signal.set("value2[%(index)d]"    % vars(), kernel.split("-")[0])
        signal.set("value3[%(index)d]"    % vars(), str(l1) + " " + str(l5) + \
                    " " + str(l15))
        signal.set("value4[%(index)d]"    % vars(), users)
        signal.set("value5[%(index)d]"    % vars(), str(d) + "d " + \
                    str(h) + "h " + str(m) + "m ")
        signal.set("value6[%(index)d]"    % vars(), str(idle_d) + "d " + \
                    str(idle_h) + "h " + str(idle_m) + "m ")
        signal.set("fill[%(index)d]"      % vars(), "%.2f" % idle_p)
        signal.set("show1[%(index)d]"     % vars(), True)
        signal.set("show2[%(index)d]"     % vars(), True)
        signal.set("show3[%(index)d]"     % vars(), True)
        signal.set("show4[%(index)d]"     % vars(), True)
        signal.set("show5[%(index)d]"     % vars(), True)
        signal.set("show6[%(index)d]"     % vars(), True)
        signal.set("showgauge[%(index)d]" % vars(), True)
        signal.set("showplot[%(index)d]"  % vars(), False)



    def set_cpu(self, signal, index):

        cpu_type    = lib.cpu.get_model()
        cpu_speed   = lib.cpu.get_speed()
        cpu_cache   = lib.cpu.get_cache_size()
        total, load = lib.cpu.get_load()

        signal.set("value1[%(index)d]"    % vars(), cpu_type)
        signal.set("value2[%(index)d]"    % vars(), cpu_speed)
        signal.set("value3[%(index)d]"    % vars(), cpu_cache)
        signal.set("fill[%(index)d]"      % vars(), load)
        signal.set("plot[%(index)d]"      % vars(), load)
        signal.set("show1[%(index)d]"     % vars(), True)
        signal.set("show2[%(index)d]"     % vars(), True)
        signal.set("show3[%(index)d]"     % vars(), True)
        signal.set("showgauge[%(index)d]" % vars(), True)
        signal.set("showplot[%(index)d]"  % vars(), True)


    def set_mem(self, signal, index):

        mem = self.__main.get_config("memory")
        if (mem == "RAM"):
            total, used = lib.memory.get_ram()
        else:
            total, used = lib.memory.get_swap()

        free = total - used

        if (total != 0):
            perc = (used / float(total)) * 100
        else:
            perc = 0

        signal.set("value1[%(index)d]"    % vars(), mem)
        signal.set("value2[%(index)d]"    % vars(),
                    lib.convert.human_readable_bytes(total))
        signal.set("value3[%(index)d]"    % vars(),
                    lib.convert.human_readable_bytes(used))
        signal.set("value4[%(index)d]"    % vars(),
                    lib.convert.human_readable_bytes(free))
        signal.set("fill[%(index)d]"      % vars(), "%.2f" % perc)
        signal.set("show1[%(index)d]"     % vars(), True)
        signal.set("show2[%(index)d]"     % vars(), True)
        signal.set("show3[%(index)d]"     % vars(), True)
        signal.set("show4[%(index)d]"     % vars(), True)
        signal.set("showgauge[%(index)d]" % vars(), True)
        signal.set("showplot[%(index)d]"  % vars(), False)



    def set_hdd(self, signal, index):

        part              = self.__main.get_config("partition")
        fs_type           = lib.disk.get_fs(part)
        total, used       = lib.disk.get_size(part)
        perc              = (used / total) * 100

        free = total - used

        signal.set("value1[%(index)d]"    % vars(), part + " (" + fs_type + ")")
        signal.set("value2[%(index)d]"    % vars(),
                   lib.convert.human_readable_bytes(total))
        signal.set("value3[%(index)d]"    % vars(),
                   lib.convert.human_readable_bytes(used))
        signal.set("value4[%(index)d]"    % vars(),
                   lib.convert.human_readable_bytes(free))
        signal.set("fill[%(index)d]"      % vars(), "%.2f" % perc)
        signal.set("show1[%(index)d]"     % vars(), True)
        signal.set("show2[%(index)d]"     % vars(), True)
        signal.set("show3[%(index)d]"     % vars(), True)
        signal.set("show4[%(index)d]"     % vars(), True)
        signal.set("showgauge[%(index)d]" % vars(), True)
        signal.set("showplot[%(index)d]"  % vars(), False)



    def set_net(self, signal, index):

        net                 = self.__main.get_config("network")
        ipaddr              = lib.net.get_ipaddr(net)
        pack_in, pack_out   = lib.net.get_pack_in_out(net)
        bytes_in, bytes_out = lib.net.get_bytes_in_out(net)
        speed_in, speed_out = lib.net.get_speed_in_out(net)

        signal.set("value1[%(index)d]"    % vars(), net)
        signal.set("value2[%(index)d]"    % vars(), ipaddr)
        signal.set("value3[%(index)d]"    % vars(),
                   lib.convert.human_readable_bytes(bytes_in))
        signal.set("value4[%(index)d]"    % vars(),
                   lib.convert.human_readable_bytes(bytes_out))
        signal.set("value5[%(index)d]"    % vars(), pack_in)
        signal.set("value6[%(index)d]"    % vars(), pack_out)
        signal.set("plot1[%(index)d]"     % vars(), speed_in)
        signal.set("plot2[%(index)d]"     % vars(), speed_out)
        signal.set("show1[%(index)d]"     % vars(), True)
        signal.set("show2[%(index)d]"     % vars(), True)
        signal.set("show3[%(index)d]"     % vars(), True)
        signal.set("show4[%(index)d]"     % vars(), True)
        signal.set("show5[%(index)d]"     % vars(), True)
        signal.set("show6[%(index)d]"     % vars(), True)
        signal.set("showgauge[%(index)d]" % vars(), False)
        signal.set("showplot[%(index)d]"  % vars(), True)
