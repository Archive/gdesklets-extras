from sensor.Sensor import Sensor
from utils.datatypes import *
from utils import i18n

from Reader import Reader

import time
import Queue

class SysInfo(Sensor):

    def __init__(self):

        global _; _ = i18n.Translator("sysinfo-sensor")

        # the maximum number of fields
        self.__array_size = 5

        self.__fonts       = ""
        self.__backgr      = ""
        self.__key_color   = ""
        self.__val_color   = ""
        self.__title_color = ""

        self.__current_size = 0
        self.__size_changed = False

        self.__reader = Reader(self)
        self.__queue  = Queue.Queue(1)

        Sensor.__init__(self)

        self._set_config_type("fonts", TYPE_STRING, "Sans 10")
        self._set_config_type("key_color", TYPE_STRING, "white")
        self._set_config_type("val_color", TYPE_STRING, "yellow")
        self._set_config_type("title_color", TYPE_STRING, "orange")
        self._set_config_type("background", TYPE_STRING, "gfx/bg.png")
        self._set_config_type("memory", TYPE_STRING, "RAM")
        self._set_config_type("partition", TYPE_STRING, "/")
        self._set_config_type("network", TYPE_STRING, "lo")

        self.add_thread(self.__get_sys_info_thread)
        self.add_timer(250, self.__get_sys_info)



    def get_configurator(self):

        part = self.__reader.get_partitions()
        net  = self.__reader.get_network()
        mem  = [['RAM', 'RAM'], ['Swap', 'Swap']]

        configurator = self._new_configurator()

        configurator.set_name(_("System Information Desklet"))
        configurator.add_title(_("Settings"))
        configurator.add_file_selector(_("Background:"), "background",
                                       _("Selects the background image file."))
        configurator.add_font_selector(_("Font:"), "fonts", _("The font which "
                                       "is used to display the labels"))
        configurator.add_color_selector(_("Key color:"), "key_color",
                                        _("The color of the key"))
        configurator.add_color_selector(_("Value color:"), "val_color",
                                        _("The color of the value"))
        configurator.add_color_selector(_("Title color:"), "title_color",
                                        _("The color of the titles"))
        configurator.add_option(_("Partition:"), "partition",
                                "Specify partition type", part)
        configurator.add_option(_("Memory:"), "memory",
                                "Specify whether RAM or Swap should be "
                                "displayed", mem)
        configurator.add_option(_("Network:"), "network",
                                "Specify network device", net)

        return configurator



    def __update_config(self):

        index = 0

        fonts       = self.get_config("fonts")
        key_color   = self.get_config("key_color")
        val_color   = self.get_config("val_color")
        title_color = self.get_config("title_color")
        background  = self.get_config("background")

        v1 = (self.__fonts, self.__key_color, self.__val_color,
              self.__title_color, self.__backgr)
        v2 = (fonts, key_color, val_color, title_color, background)

        # only take action when some value has changed
        if (v1 != v2 or self.__size_changed):
            signal = self.new_output()

            signal.set("icon[%(index)d]" % vars(), "gfx/Info.png")
            signal.set("key1[%(index)d]" % vars(), (""))
            signal.set("key2[%(index)d]" % vars(), _("Kernel:"))
            signal.set("key3[%(index)d]" % vars(), _("Load:"))
            signal.set("key4[%(index)d]" % vars(), _("Users:"))
            signal.set("key5[%(index)d]" % vars(), _("Uptime:"))
            signal.set("key6[%(index)d]" % vars(), _("Idle time:"))
            index += 1
            signal.set("icon[%(index)d]" % vars(), "gfx/cpu.png")
            signal.set("key1[%(index)d]" % vars(), (""))
            signal.set("key2[%(index)d]" % vars(), _("Clock:"))
            signal.set("key3[%(index)d]" % vars(), _("Bogomips:"))
            signal.set("key4[%(index)d]" % vars(), _("Cache:"))
            signal.set("key5[%(index)d]" % vars(), _("Load:"))
            index += 1
            signal.set("icon[%(index)d]" % vars(), "gfx/memory.png")
            signal.set("key1[%(index)d]" % vars(), (""))
            signal.set("key2[%(index)d]" % vars(), _("Total:"))
            signal.set("key3[%(index)d]" % vars(), _("Used:"))
            signal.set("key4[%(index)d]" % vars(), _("Free:"))
            signal.set("key5[%(index)d]" % vars(), _("Buffer:"))
            signal.set("key6[%(index)d]" % vars(), _("Cached:"))
            index += 1
            signal.set("icon[%(index)d]" % vars(), "gfx/hdd.png")
            signal.set("key1[%(index)d]" % vars(), (""))
            signal.set("key2[%(index)d]" % vars(), _("Total:"))
            signal.set("key3[%(index)d]" % vars(), _("Used:"))
            signal.set("key4[%(index)d]" % vars(), _("Free:"))
            index += 1
	    """
            signal.set("icon[%(index)d]" % vars(), "gfx/network.png")
            signal.set("key1[%(index)d]" % vars(), (""))
            signal.set("key2[%(index)d]" % vars(), _("IP addr:"))
            signal.set("key3[%(index)d]" % vars(), _("Bytes in:"))
            signal.set("key4[%(index)d]" % vars(), _("Bytes out:"))
            signal.set("key5[%(index)d]" % vars(), _("Packets in:"))
            signal.set("key6[%(index)d]" % vars(), _("Packets out:"))
            index += 1
	    """
            if (self.__backgr != background):
                signal.set("background", background)
            if (self.__fonts != fonts or self.__size_changed):
                for i in xrange(self.__array_size):
                    signal.set("fonts[%(i)d]" %vars(), fonts)
            if (self.__key_color != key_color or self.__size_changed):
                for i in xrange(self.__array_size):
                    signal.set("key_color[%(i)d]" %vars(), key_color)
            if (self.__val_color != val_color or self.__size_changed):
                for i in xrange(self.__array_size):
                    signal.set("val_color[%(i)d]" %vars(), val_color)
            if (self.__title_color != title_color or self.__size_changed):
                for i in xrange(self.__array_size):
                    signal.set("title_color[%(i)d]" %vars(), title_color)

            self.send_output(signal)

            self.__fonts        = fonts
            self.__key_color    = key_color
            self.__val_color    = val_color
            self.__title_color  = title_color
            self.__backgr       = background
            self.__size_changed = False



    def __get_sys_info(self):

        self.__update_config()
        if (not self.__queue.empty()):
            signal = self.__queue.get()
            self.send_output(signal)

            size = signal.get("size")
            if (size != self.__current_size):
                self.__current_size = size
                self.__size_changed = True

        return True



    def __get_sys_info_thread(self):

        while (True):
            index = 0

            if (self.is_stopped()): break

            signal = self.new_output()

            try:
                self.__reader.set_sys(signal, index)
                index += 1
                self.__reader.set_cpu(signal, index)
                index += 1
                self.__reader.set_mem(signal, index)
                index += 1
                self.__reader.set_hdd(signal, index)
                index += 1
                #self.__reader.set_net(signal, index)
                #index += 1
            except:
                import traceback; traceback.print_exc()

            signal.set("size", index)

            self.__queue.put(signal)
            time.sleep(1)



def new_sensor(args): return SysInfo( *args )
