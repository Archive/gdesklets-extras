from sensor.Sensor import Sensor

import commands, os

#
# Sensor for watching wireless connection status
#
class WirelessSensor(Sensor):

    def __init__(self):

        Sensor.__init__(self)
        self.add_timer(0, self.__get_data)

    def __get_data(self):
        try:
          data = os.popen( 'cat /proc/net/wireless' ).read().splitlines()[2]
         
          # I currently don't know what the wifi strength gnome applet uses, but I cannot match it...
 
          strength = float(data.split()[3])/256.0 # this should be the percentage strengh (1.0 == 100%)
          iface = data.split()[0][:-1]

          signal = self.new_output()
          signal.set("strength", "%s%%" % int(strength*100) )
          signal.set("iface", iface )
          signal.set("width", int(36.0*strength) ) # at 100% width == 36

          self.send_output(signal)
        except:
          signal = self.new_output()
          signal.set("strength", "0%" )
          signal.set("iface", "N/A")
          signal.set("width", 0 )
          self.send_output(signal)

        self.add_timer(3000, self.__get_data)

def new_sensor(args): return WirelessSensor( *args )
