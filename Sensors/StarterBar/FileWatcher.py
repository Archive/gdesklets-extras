from utils.Observable import Observable

try:
    import gnome.vfs as vfs
    HAVE_VFS = True

except ImportError:
    HAVE_VFS = False

import gtk
import os


#
# Class for reporting when a file has changed.
#
class FileWatcher(Observable):

    OBS_CHANGE = 0

    def __init__(self, path):

        self.__path = path
        self.__mtime = 0

        if (HAVE_VFS):
            vfs_path = "file://" + self.__path
            try:
                vfs.monitor_add(vfs_path, vfs.MONITOR_FILE,
                                self.update_observer, self.OBS_CHANGE)
            except:
                gtk.timeout_add(500, self.__check_mtime)
        else:
            gtk.timeout_add(500, self.__check_mtime)



    def __check_mtime(self):

        try:
            new_mtime = os.path.getmtime(self.__path)
            if (new_mtime != self.__mtime):
                self.__mtime = new_mtime
                self.update_observer(self.OBS_CHANGE, self.__path)
        except OSError:
            return gtk.FALSE

        return gtk.TRUE
