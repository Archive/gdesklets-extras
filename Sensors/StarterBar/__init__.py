from sensor.Sensor import Sensor
from IconSet import IconSet
from utils.datatypes import *
from utils import i18n

import os
import commands
import math
import gtk


#
# Iconbar for starters (.desktop files).
#
class StarterBar(Sensor):

    # panel orientation values
    TOP = "top"
    BOTTOM = "bottom"
    LEFT = "left"
    RIGHT = "right"

    # panel geometries (x, y, width, height, center pos)
    GEOMETRY = {TOP: (32, 0, -1, 42, 0),
                BOTTOM: (32, 56, -1, 42, 100),
                LEFT: (0, 32, 42, -1, 0),
                RIGHT: (56, 32, 42, -1, 100)}


    def __init__(self):

        global _; _ = i18n.Translator("starterbar-sensor")

        # the current pointer position
        self.__current_pointer_pos = (0, 0)
        self.__last_pointer_pos = (0, 0)

        # whether we're showing a caption atm
        self.__showing_caption = 0

        # the zoom and align data of the icons; we remember it for not
        # redrawing too much
        self.__animation_states = []

        # the currently launching icons
        self.__launching_icons = []

        # the set of icons
        self.__icon_set = None        

        # the index of the currently selected icon or -1
        self.__current_index = -1

        # the index of the currently dragged icon or -1
        self.__current_drag_index = -1

        # the current orientation of the panel
        self.__orientation = self.BOTTOM

        # whether to display animations
        self.__has_animations = True

        # whether to stretch the panel
        self.__has_panel_stretch = True

        # the jump align for icons
        self.__jump_align = -1

        # the scaling of the panel
        self.__panel_scaling = 0.5

        # the size of the panel
        self.__panel_size = 0

        # the center of the panel (used for distance calculations)
        self.__panel_center = 0

        # the current stretching factor of the panel
        self.__stretching_factor = 0.0
        

        Sensor.__init__(self)
        self._set_config_type("orientation", TYPE_STRING, self.__orientation)
        self._set_config_type("animation", TYPE_BOOL, True)
        self._set_config_type("stretch", TYPE_BOOL, False)
        self._set_config_type("panel-scaling", TYPE_INT, 100)
        self._set_config_type("background", TYPE_STRING, "gfx/bg.png")
        self._set_config_type("foreground", TYPE_STRING, "")
        self._set_config_type("caption-color", TYPE_STRING, "yellow")
        self._set_config_type("caption-font", TYPE_STRING, "Serif 12")
        self._set_config_type("captions", TYPE_BOOL, True)

        self.watch_config(self.__on_watch_config)

        self.add_timer(0, self.__initialize)



    #
    # Initializes the bar.
    #
    def __initialize(self):

        self.__icon_set = IconSet(self.get_config_id(), self.get_path())
        self.set_paths_to_purge([self.__icon_set.get_icon_path()])

        # set up callbacks
        self.__icon_set.set_icon_changed_handler(self.__set_icons)
        self.__icon_set.set_size_changed_handler(self.__setup_panel)
        
        self.add_timer(0, self.__icon_set.load_icons)
        self.add_timer(0, self.__animation_timer)

        # read in initial configuration
        self.__has_animations = self.get_config("animation")
        self.__has_panel_stretch = self.get_config("stretch")
        self.__orientation = self.get_config("orientation")
        self.__panel_scaling = self.get_config("panel-scaling") / 100.0



    def __zoom_function(self, x):

        a = -0.90 #-0.45
        b = 2.0 #1.9 #1.6

        return (a * x*x + b)



    def __align_function(self, x):

        a = 0.1
        b = 0.0
        c = 0.0

        return max(-0.5, min(0.5, 1.0 * x)) #a * x*x + b*x + c))


    #
    # Timer for displaying the animations.
    #
    def __animation_timer(self):

        if (self.__has_animations
              and self.__current_pointer_pos != self.__last_pointer_pos):
            orientation = self.__orientation
            x, y = self.__current_pointer_pos
            self.__last_pointer_pos = self.__current_pointer_pos

            if ((x, y) < (0, 0)):
                position = 0
                distance = 1000
            
            elif (orientation in [self.TOP, self.BOTTOM]):
                position = x
                distance = abs(self.__panel_center - y)
            else:
                position = y
                distance = abs(self.__panel_center - x)

            if (distance < 0.0001): distance += 0.1
            factor = (56.0 * self.__panel_scaling) / distance - 0.4
            factor = max(0, min(1, factor))
            
            if (self.__has_panel_stretch and self.__current_drag_index == -1):
                self.__set_stretching(factor)
            
            length = self.__icon_set.get_length()            
            index = (position / float(self.__panel_size)) * length
            self.__animate(index - 0.5, factor)

        self.add_timer(30, self.__animation_timer)


    #
    # Timer for displaying the launching feedback animation.
    # TODO: make use of startup-notification protocol
    #
    def __launch_animation_timer(self, index, pos = 0.0):

        pos += math.pi / 12.0
        value = abs(math.sin(0.9 * pos))

        output = self.new_output()
        output.set("icon-scale[%d]" % index, self.__panel_scaling)
        if (self.__orientation in [self.TOP, self.BOTTOM]):
            output.set("icon-align-y[%d]" % index,
                       abs(self.__jump_align + value))
        elif (self.__orientation in [self.LEFT, self.RIGHT]):
            output.set("icon-align-x[%d]" % index,
                       abs(self.__jump_align + value))

        self.send_output(output)

        if (pos < 3.25 * math.pi):
            self.add_timer(50, self.__launch_animation_timer, index, pos)
        else:
            self.__launching_icons.remove(index)
            
        return gtk.FALSE


    #
    # Animates the icons.
    #
    def __animate(self, index, factor):

        damping = factor
        damping5 = damping * damping * damping * damping * damping
        f = self.__zoom_function
        g = self.__align_function

        output = self.new_output()

        length = self.__icon_set.get_length()
        for i in range(length):
            if (self.__current_drag_index != -1):
                # scale the currently dragged icon larger
                zoom = (i == self.__current_drag_index) and 1.5 or 1.0
                align = 0.0
            else:
                zoom = max(1.0, f(i - index) * damping)
                align = g(i - index) * damping5

            prev_zoom, prev_align = self.__animation_states[i]
            if (prev_zoom != zoom and not i in self.__launching_icons):
                output.set("icon-scale[%d]" % i, zoom * self.__panel_scaling)
            if (prev_align != align and
                  self.__orientation in [self.TOP, self.BOTTOM]):
                output.set("icon-align-x[%d]" % i, 0.5 + align)
            elif (prev_align != align and
                  self.__orientation in [self.LEFT, self.RIGHT]):
                output.set("icon-align-y[%d]" % i, 0.5 + align)
            self.__animation_states[i] = (zoom, align)
        #end for

        self.send_output(output)



    #
    # Displays the icons.
    #
    def __set_icons(self):

        length = self.__icon_set.get_length()
        output = self.new_output()
        for i in range(length):
            icon = self.__icon_set.get_icon(i)
            output.set("icon-uri[%d]" % i,
                       icon.get_pixmap() or \
                       os.path.join(self.get_path(), "unknown.png"))

        self.send_output(output)

    
    #
    # Reacts on changes in the configuration.
    #
    def __on_watch_config(self, key, value):

        if (key == "animation"):
            self.__has_animations = self.get_config("animation")
            self.__setup_panel()
            
        elif (key == "background"):
            output = self.new_output()
            output.set("background", value)
            self.send_output(output)

        elif (key == "caption-color"):
            output = self.new_output()
            output.set("caption-color", value)
            self.send_output(output)

        elif (key == "caption-font"):
            output = self.new_output()
            output.set("caption-font", value)
            self.send_output(output)

        elif (key == "captions"):
            self.__setup_panel()

        elif (key == "foreground"):
            output = self.new_output()
            output.set("foreground", value)
            self.send_output(output)

        elif (key == "orientation"):
            self.__orientation = value
            self.__setup_panel()

        elif (key == "panel-scaling"):
            self.__panel_scaling = float(value) / 100.0
            self.__setup_panel()

        elif (key == "stretch"):
            self.__has_panel_stretch = self.get_config("stretch")
            self.__setup_panel()
        

    #
    # Sets up the panel, i.e. orientation and icon size.
    #
    def __setup_panel(self):

        self.__set_orientation(self.__orientation)

        length = self.__icon_set.get_length()
        output = self.new_output()
        for i in xrange(length):
            output.set("icon-scale[%d]" % i, self.__panel_scaling)

            if (self.__orientation in [self.TOP, self.BOTTOM]):
                output.set("icon-align-x[%d]" % i, 0.5)
                output.set("icon-align-y[%d]" % i, abs(self.__jump_align))
            elif (self.__orientation in [self.LEFT, self.RIGHT]):
                output.set("icon-align-y[%d]" % i, 0.5)
                output.set("icon-align-x[%d]" % i, abs(self.__jump_align))
        #end for

        self.__animation_states = [(-1, -1)] * length
        output.set("length", length)
        output.set("background", self.get_config("background"))
        output.set("foreground", self.get_config("foreground"))
        output.set("caption-color", self.get_config("caption-color"))
        output.set("caption-font", self.get_config("caption-font"))
        self.send_output(output)



    #
    # Sets the orientation of the panel.
    #
    def __set_orientation(self, orientation):

        length = self.__icon_set.get_length()
        
        distance = int(56 * self.__panel_scaling)
        size = max(32, length * 56)
        totalsize = (length) * 56 + (120 - 56)
        
        output = self.new_output()
        panel_x, panel_y, panel_width, panel_height, center = \
                 self.GEOMETRY[orientation]
        if (orientation in [self.TOP, self.LEFT]):
            self.__jump_align = 0
        else:
            self.__jump_align = -1

        if (orientation in [self.TOP, self.BOTTOM]):
            layout = "horizontal, %d" % distance
            panel_width = size
            total_width, total_height = totalsize, 100
            align_width = 120
            align_height = 100

        else:
            layout = "vertical, %d" % distance
            panel_height = size
            total_width, total_height = 100, totalsize
            align_width = 100
            align_height = 120

        if (not self.get_config("captions")):
            output.set("caption1-visible", "false")
            output.set("caption2-visible", "false")
        elif (orientation == self.TOP):
            output.set("caption1-visible", "false")
            output.set("caption2-visible", "true")
        elif (orientation == self.BOTTOM):
            output.set("caption1-visible", "true")
            output.set("caption2-visible", "false")
        else:
            output.set("caption1-visible", "false")
            output.set("caption2-visible", "false")

        scale = self.__panel_scaling
        output.set("layout", layout)
        output.set("panel-x", panel_x * scale)
        output.set("panel-y", panel_y * scale)
        output.set("panel-width", panel_width * scale)
        output.set("panel-height", panel_height * scale)
        output.set("fg-x", panel_x * scale + 4)
        output.set("fg-y", panel_y * scale + 4)
        output.set("fg-width", panel_width * scale - 8)
        output.set("fg-height", panel_height * scale - 8)
        
        for i in xrange(self.__icon_set.get_length()):
            output.set("align-width[%d]" % i, align_width * scale)
            output.set("align-height[%d]" % i, align_height * scale)

        output.set("total-width", total_width * scale)
        output.set("total-height", total_height * scale)
        
        self.send_output(output)
        self.__panel_size = totalsize * scale
        self.__panel_center = center * scale



    #
    # Stretches/unstretches the panel.
    #
    def __set_stretching(self, factor):

        if (abs(self.__stretching_factor - factor) < 0.1): return
        self.__stretching_factor = factor
        
        orientation = self.__orientation
        factor = max(0.0, min(32.0, factor))
        stretch = 32 * factor
        
        length = self.__icon_set.get_length()
        size = max(32, length * 56)
        
        output = self.new_output()
        panel_x, panel_y, panel_width, panel_height, center = \
                 self.GEOMETRY[orientation]
        if (orientation == self.TOP):
            panel = (32 - stretch, panel_y, size + 2 * stretch, panel_height)
        elif (orientation == self.BOTTOM):
            panel = (32 - stretch, panel_y, size + 2 * stretch, panel_height)
        elif (orientation == self.LEFT):
            panel = (panel_x, 32 - stretch, panel_width, size + 2 * stretch)
        elif (orientation == self.RIGHT):
            panel = (panel_x, 32 - stretch, panel_width, size + 2 * stretch)

        scale = self.__panel_scaling
        output.set("panel-x", panel[0] * scale)
        output.set("panel-y", panel[1] * scale)
        output.set("panel-width", panel[2] * scale)
        output.set("panel-height", panel[3] * scale)
        output.set("fg-x", panel[0] * scale + 4)
        output.set("fg-y", panel[1] * scale + 4)
        output.set("fg-width", panel[2] * scale - 8)
        output.set("fg-height", panel[3] * scale - 8)
        self.send_output(output)



    #
    # Displays the given caption at the given position.
    #
    def __show_caption(self, x, caption):

        output = self.new_output()
        output.set("caption-x", x)
        output.set("caption", caption)
        self.send_output(output)
        self.__showing_caption = 1



    #
    # Hides the caption.
    #
    def __hide_caption(self):

        output = self.new_output()
        output.set("caption", "")
        self.send_output(output)
        self.__showing_caption = 0


    #
    # Checks if a tooltip has to be displayed and displays it.
    #
    def __check_for_tooltip(self, x, y):

        if ((x, y) == self.__current_pointer_pos and
              self.__current_index != -1):
            x = x / self.__panel_size
            icon = self.__icon_set.get_icon(self.__current_index)
            name = icon.get_name() or icon.get_command()
            name = name.replace("&", "&amp;").replace("<", "&lt;")
            self.__show_caption(x, name)

        return gtk.FALSE


    #
    # Reacts on user actions.
    #
    def call_action(self, action, path, args = []):

        if (self.__showing_caption): self.__hide_caption()

        # mouse motion
        if (action == "motion"):
            x = int(args[0])
            y = int(args[1])
            self.__current_pointer_pos = (x, y)

            if (not self.__showing_caption):
                self.add_timer(300, self.__check_for_tooltip, x, y)

            if (self.__current_index != self.__current_drag_index != -1):
                if (self.__current_drag_index > self.__current_index):
                    self.__icon_set.move_icon(self.__current_drag_index,
                                              self.__current_index)
                else:
                    self.__icon_set.move_icon(self.__current_drag_index,
                                              self.__current_index + 1)

                self.__current_drag_index = self.__current_index

        # enter icon
        elif (action == "enter-icon"):
            index = path[-1]
            self.__current_index = index

        # enter icon panel
        elif (action == "enter"):
            self.__current_drag_index = -1
            
        # leave icon panel
        elif (action == "leave"):
            self.__current_pointer_pos = (-1, -1)
            self.__current_index = -1
            self.__current_drag_index = -1

        # start application
        elif (action == "launch"):
            index = self.__current_index
            print index, self.__launching_icons
            if (index != -1 and not index in self.__launching_icons):
                self.__launching_icons.append(index)
                self.__launch_icon(index)
                if (self.__has_animations):
                    self.add_timer(0, self.__launch_animation_timer, index)
                else:
                    self.__launching_icons.remove(index)
            #end if
            self.__current_pointer_pos = (-1, -1)
            self.__current_drag_index = -1
            
        # open menu
        elif (action == "menu"):
            removable = (self.__current_index != -1)
            self.__current_pointer_pos = (-1, -1)
            self.__current_drag_index = -1

            menu = [("New starter...", 1, None, self.__add_starter, []),
                    ("Edit starter...", removable, None, self.__edit_starter,
                     []),
                    ("Remove starter", removable, None, self.__remove_starter,
                     [])
                    ]
            self.open_menu(menu)

        # add icon
        elif (action == "add"):
            for file in args[0]:
                self.__icon_set.add_icon(file)

        # launch with file as argument
        elif (action == "launch-with"):
            index = path[-1]
            self.__current_index = index
            if (not index in self.__launching_icons):
                self.__launching_icons.append(index)
                for file in args[0]:
                    self.__launch_icon(index, file)
                if (self.__has_animations):
                    self.add_timer(0, self.__launch_animation_timer, index)
            #end if

        # drag icon
        elif (action == "drag-icon"):
            self.__current_drag_index = self.__current_index

        # drop icon
        elif (action == "drop-icon"):
            self.__current_drag_index = -1


    #
    # Launches the given icon with the given file as argument.
    #
    def __launch_icon(self, index, file = ""):

        cmd = self.__icon_set.get_icon(index).get_command()
        cmd = cmd.replace("%U", file)

        os.system("cd ~ && exec " + cmd + " & disown")


    #
    # Adds a new starter.
    #
    def __add_starter(self, src, *args):

        self.__icon_set.add_icon()



    #
    # Removes the given starter.
    #
    def __remove_starter(self, src):

        index = self.__current_index
        if (index == -1): return
        self.__icon_set.remove_icon(index)



    #
    # Edits the given starter.
    #
    def __edit_starter(self, src):

        index = self.__current_index
        if (index == -1): return
        self.__icon_set.edit_icon(index)



    #
    # Returns the configuration GUI.
    #
    def get_configurator(self):

        configurator = self._new_configurator()
        configurator.set_name(_("Panel"))
        configurator.add_title(_("Appearance"))
        configurator.add_option(_("Orientation:"), "orientation",
                                _("The orientation of the panel"),
                                [(_("top"), self.TOP),
                                 (_("bottom"), self.BOTTOM),
                                 (_("left"), self.LEFT),
                                 (_("right"), self.RIGHT)]
                                )
        configurator.add_spin(_("Zoom:"), "panel-scaling",
                              _("The size of the panel in percents"),
                              10, 500)
        configurator.add_checkbox(_("Show animations"), "animation",
                                  _("Toggles eye candy animations on or off"))
        configurator.add_checkbox(_("Stretch panel (only when animated)"),
                                  "stretch",
                                  _("Toggles stretching of the panel on "
                                    "or off"))
        configurator.add_checkbox(_("Show captions (only in horizontal orientation)"),
                                  "captions",
                                  _("Toggles captions on or off"))
        configurator.add_font_selector(_("Caption Font:"), "caption-font",
                                       _("Selects the caption's font."))
        configurator.add_color_selector(_("Caption Color:"), "caption-color",
                                        _("Selects the caption's color."))
        configurator.add_file_selector(_("Background:"), "background",
                                       _("Selects the background image file."))
        configurator.add_file_selector(_("Foreground:"), "foreground",
                                       _("Selects the foreground image file."))

        return configurator



    #
    # Executes shutdown tasks.
    #
    def _shutdown(self):

        pass



def new_sensor(args): return StarterBar( *args )
