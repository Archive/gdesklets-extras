from Icon import Icon
try:
    # gDesklets >= 0.30
    from utils.FileWatcher import FileWatcher
    from utils.GConfWatcher import GConfWatcher
except:
    # gDesklets <= 0.26.2
    from utils import FileWatcher
    from utils import GConfWatcher


import commands
import os
import gnome.vfs
import gconf


class IconSet:

    # the editor for starters
    __EDITOR = "gnome-desktop-item-edit"

    # where the starters are stored
    __ICONPATH = os.path.expanduser("~/.gdesklets/MyStarters/%(set_id)s")



    def __init__(self, set_id, path):

        # the ID of the icon set to use
        self.__set_id = set_id

        self.__path = path

        # the icons
        self.__icons = []

        # the desktop files to load
        self.__to_load = []

        # the indexes of the icons that are to be reloaded
        self.__to_reload = []

        # the order of the icons
        self.__order = []

        # the icon-changed handler
        self.__handler_icon_changed = None

        # the size-changed handler
        self.__handler_size_changed = None

        # the icon theme
        self.__theme = ""


        self.__iconpath = os.path.expanduser(self.__ICONPATH % vars())
        self.__orderfile = os.path.join(self.__iconpath, ".order")

        self.__initialize()
        watcher = FileWatcher(self.__orderfile)
        watcher.add_observer(self.__on_observe_icons)

        gconfwatcher = GConfWatcher("/desktop/gnome/interface/icon_theme")
        gconfwatcher.add_observer(self.__on_observe_theme)

        client = gconf.client_get_default()
        self.__theme = \
               client.get("/desktop/gnome/interface/icon_theme").get_string()



    #
    # Initializes the icon set, if necessary.
    #
    def __initialize(self):

        set_id = self.__set_id

        # create icon dir, if necessary
        if (not os.path.exists(self.__iconpath)):
            os.makedirs(self.__iconpath)

        # create order file, if necessary
        if (not os.path.exists(self.__orderfile)):
            cwd = self.__path #os.getcwd()
            iconpath = self.__iconpath
            file1 = os.path.join(cwd, "home.desktop")
            file2 = os.path.join(cwd, ".order")
            os.system("cp %(file1)s %(file2)s %(iconpath)s/" % vars())
            print "cp %(file1)s %(file2)s %(iconpath)s/" % vars()
            #os.system("touch " + self.__orderfile)



    #
    # Copies a file between two VFS locations.
    #
    def __vfscopy(self, src, dest):

        print "copying %(src)s -> %(dest)s" % vars()
        file = gnome.vfs.URI(dest)
        if (gnome.vfs.exists(file)):
            fd = gnome.vfs.open(file, gnome.vfs.OPEN_WRITE)
        else:
            fd = gnome.vfs.create(file, gnome.vfs.OPEN_WRITE)
        fdsrc = gnome.vfs.open(src, gnome.vfs.OPEN_READ)
        data = ""
        while (1):
            try:
                data += fdsrc.read(1)
            except gnome.vfs.EOFError:
                break

        fd.write(data)
        fd.close()
        fdsrc.close()



    #
    # Closes this icon set.
    #
    def close(self):

        # delete the directory
        if (os.path.exists(self.__iconpath)):
            os.system("rm -rf " + self.__iconpath)



    #
    # Returns the ID of the icon set.
    #
    def get_set_id(self): return self.__set_id



    #
    # Sets the ID of the icon set.
    #
    def set_set_id(self):

        pass




    #
    # (Re)loads all icons.
    #
    def load_icons(self):

        self.__order = self.__load_order()
        self.__to_load = self.__order[:]
        self.__icons = []
        self.__load_new_icons()


    #
    # Loads new icons.
    #
    def __load_new_icons(self):

        while (self.__to_load):
            filename = self.__to_load.pop(0)
            path = os.path.join(self.__iconpath, filename)

            icon = Icon(path, self.__theme)
            self.__icons.append(icon)
        #end for

        if (self.__handler_icon_changed): self.__handler_icon_changed()
        if (self.__handler_size_changed): self.__handler_size_changed()


    #
    # Reloads the given icon.
    #
    def __reload_icon(self, index):

        filename = self.__order[index]
        path = os.path.join(self.__iconpath, filename)

        icon = Icon(path, self.__theme)
        self.__icons[index] = icon
        if (self.__handler_icon_changed): self.__handler_icon_changed()


    #
    # Reacts on changes of the icons.
    #
    def __on_observe_icons(self, src, cmd, *args):

        changed = 0
        while (self.__to_reload):
            self.__reload_icon(self.__to_reload.pop())
            changed = 1

        if (changed or self.__to_load): self.__load_new_icons()


    #
    # Observes the icon theme setting.
    #
    def __on_observe_theme(self, src, cmd, entry):

        self.__theme = entry.get_value().get_string()
        # this ugly hack is necessary because xdg is buggy when changing the
        # icon theme and doesn't wanna forget about the old theme
        from xdg import IconTheme; IconTheme.themes = []
        self.load_icons()



    #
    # Loads the order of icons.
    #
    def __load_order(self):

        order = []

        fd = open(self.__orderfile, "r")
        for i in fd.readlines():
            # ignore empty lines
            if (i == "\n"): continue

            # drop the trailing newline
            if (i[-1] == "\n"): order.append(i[:-1])
            else: order.append(i)
        fd.close()

        return order



    #
    # Stores the order of icons.
    #
    def __store_order(self, order):

        fd = open(self.__orderfile, "w")
        fd.write("\n".join(order))
        fd.write("\n")
        fd.close()





    #
    # Adds a new icon and opens the icon editor.
    #
    def add_icon(self, fromFile = ""):

        name = commands.getoutput("uuidgen") + ".desktop"
        path = os.path.join(self, self.__iconpath, name)
        self.__to_load.append(name)

        if (fromFile):
            print "ADD"
            if (fromFile[-8:].lower() != '.desktop'): return

            self.__vfscopy(fromFile, path)
            cmd = "echo " + name + " >>" + self.__orderfile + " &"
        else:
            cmd = self.__EDITOR + " " + path + " && " + \
                  "echo " + name + " >>" + self.__orderfile + " &"
        os.system(cmd)

        self.__order.append(name)




    #
    # Removes the icon at the given position.
    #
    def remove_icon(self, index):

        if (index == -1): return

        name = self.__order[index]
        path = os.path.join(self.__iconpath, name)
        try:
            os.unlink(path)
        except:
            pass

        del self.__icons[index]
        self.__order.remove(name)
        self.__store_order(self.__order)
        if (self.__handler_size_changed): self.__handler_size_changed()
        if (self.__handler_icon_changed): self.__handler_icon_changed()
        print "removed"



    #
    # Opens an editor for the given icon.
    #
    def edit_icon(self, index):

        if (index == -1): return

        name = self.__order[index]
        path = os.path.join(self.__iconpath, name)

        cmd = self.__EDITOR + " " + path + " && " + \
              "touch " + self.__orderfile + " &"
        os.system(cmd)
        self.__to_reload.append(index)


    #
    # Swaps the two given icons.
    #
    def swap_icons(self, index1, index2):

        tmp = self.__order[index2]
        self.__order[index2] = self.__order[index1]
        self.__order[index1] = tmp
        self.__store_order(self.__order)


    #
    # Moves the icon from place A to place B and inserts it before B.
    #
    def move_icon(self, a, b):

        icon = self.__icons[a]
        o = self.__order[a]
        self.__icons.insert(b, icon)
        self.__order.insert(b, o)
        if (a > b):
            self.__icons.pop(a + 1)
            self.__order.pop(a + 1)
        else:
            self.__icons.pop(a)
            self.__order.pop(a)

        self.__store_order(self.__order)
        if (self.__handler_icon_changed): self.__handler_icon_changed()


    #
    # Returns the icon at the given position index.
    #
    def get_icon(self, index):

        return self.__icons[index]



    #
    # Returns the length of the icon set, i.e. the number of the icons in it.
    #
    def get_length(self):

        return len(self.__icons)



    def set_icon_changed_handler(self, handler):

        self.__handler_icon_changed = handler


    def set_size_changed_handler(self, handler):

        self.__handler_size_changed = handler



    #
    # Returns the icon path.
    #
    def get_icon_path(self):

        return self.__iconpath
