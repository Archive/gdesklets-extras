from xdg.DesktopEntry import DesktopEntry
from xdg import IconTheme
import gconf
import os
import commands
import locale; locale.setlocale(locale.LC_ALL, "")

# we want to be able to use user-installed .icon themes
IconTheme.icondirs.append(os.path.expanduser("~/.icons"))
IconTheme.icondirs.append(os.path.join(
    commands.getoutput("dirname `which gnome-desktop-item-edit`"),
    "..", "share", "icons"))
IconTheme.icondirs.append(os.path.join(
    commands.getoutput("dirname `which gnome-desktop-item-edit`"),
    "..", "share", "pixmaps"))


class Icon:

    # the unknown icon
    __UNKNOWN_ICON = "gnome-unknown"


    def __init__(self, desktopfile, theme = ""):

        self.__zoom_level = 1.0
        self.__command = ""
        self.__name = ""
        self.__pixmap = ""
        self.__theme = theme

        self.__load_icon(desktopfile)



    #
    # Loads the given icon.
    #
    def __load_icon(self, desktopfile):
        
        reader = DesktopEntry()
        reader.setLocale(locale.getlocale()[0])

        try:
            reader.parse(desktopfile)
        except:
            print "Error reading .desktop file:", desktopfile
            self.__pixmap = ""

        iconname = reader.getIcon()
        try:
            self.__pixmap = IconTheme.getIconPath(iconname, size = 256,
                                                  theme = self.__theme)
        except:
            self.__pixmap = ""

        self.__command = reader.getExec()
        self.__name = reader.getName()

        # check whether the command should be run in a terminal
        is_terminal = (reader.getTerminal().lower() == "true")
        if (is_terminal):
            client = gconf.client_get_default()
            term_cmd = client.get(
                '/desktop/gnome/applications/terminal/exec').get_string()
            term_arg = client.get(
                '/desktop/gnome/applications/terminal/exec_arg').get_string()
            cmd = '%s %s ' % (term_cmd, term_arg)
            self.__command = cmd + self.__command

    def get_pixmap(self): return self.__pixmap

    def set_zoom(self, zoom): self.__zoom_level = zoom
    def get_zoom(self): return self.__zoom_level

    def get_command(self): return self.__command
    def get_name(self): return self.__name
