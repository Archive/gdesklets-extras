from utils.Observable import Observable

import gconf


#
# Class for reporting when a GConf entry has changed. This has nothing to do
# with the GConf config backend and is meant for watching GConf entries of
# other applications.
#
class GConfWatcher(Observable):

    OBS_CHANGE = 0

    def __init__(self, path):

        self.__path = path
        self.__mtime = 0

        client = gconf.client_get_default()
        client.notify_add(path, self.__on_notify)



    def __on_notify(self, client, cid, entry, err):

        self.update_observer(self.OBS_CHANGE, entry)
